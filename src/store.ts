import createSagaMiddleware from 'redux-saga';
import storage from 'redux-persist/lib/storage';

import { createLogger } from 'redux-logger';
import { persistReducer } from 'redux-persist';
import { applyMiddleware, createStore } from 'redux';

import rootSaga from './sagas';

import { reducers } from './reducers';

const sagaMiddleware = createSagaMiddleware();

const config = {
  storage,
  key: 'root',
};

const logger = createLogger({
  collapsed: true,
});

const persistedReducer = persistReducer(config, reducers);

const createDevelopmentStore = () => {
  const store = createStore(
    persistedReducer,
    applyMiddleware(sagaMiddleware, logger),
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

export const store = createDevelopmentStore();
