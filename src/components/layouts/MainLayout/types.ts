import { RouteComponentProps } from 'react-router-dom';

export interface ComponentProps {
  logOut?: () => void;
  isEditMode?: boolean;
  isVisibleSwitch: boolean;
  isVisibleBookmarks: boolean;
  onEditMode?: (flag: boolean) => void;
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = MapStateProps
  & MapDispatchProps
  & ComponentProps
  & RouteComponentProps<{}>;
