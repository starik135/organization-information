import * as React from 'react';

import { Switch } from 'antd';
import { withRouter } from 'react-router';

import { AllProps } from './types';
import { loc } from '../../../localization';

import {
  SIGN_FORM_APP_ENDPOINT,
  MANUALS_APP_ENDPOINT,
  ACCOUNT_APP_ENDPOINT,
  ORGANIZATIONS_APP_ENDPOINT,
} from '../../../constants';

import './styles.scss';

class MainLayout extends React.Component<AllProps> {

  private relocateFromAccount = () => this.props.history.push(ACCOUNT_APP_ENDPOINT);
  private relocateFromManuals = () => this.props.history.push(MANUALS_APP_ENDPOINT);
  private relocateFromOrganizations = () => this.props.history.push(ORGANIZATIONS_APP_ENDPOINT);

  private onChange = (checked: boolean) => this.props.onEditMode && this.props.onEditMode(checked);

  private signOut = () => {
    const { logOut, history } = this.props;

    logOut && logOut();
    history.push(SIGN_FORM_APP_ENDPOINT);
  }

  public render(): JSX.Element {
    const { isVisibleSwitch, isVisibleBookmarks } = this.props;

    return (
      <React.Fragment>
        <header className="header-switch-button">
          {
            isVisibleBookmarks
              && <div className="bookmarks-wrapper">
                <button className="bookmarks-wrapper__bookmarks">
                  <p onClick={this.relocateFromOrganizations}>{loc.headerButton.organization}</p>
                </button>
                <button className="bookmarks-wrapper__bookmarks">
                  <p onClick={this.relocateFromManuals}>{loc.headerButton.manual}</p>
                </button>
              </div>
          }
          {isVisibleSwitch && <Switch checked={this.props.isEditMode} onChange={this.onChange} />}
          <button onClick={this.relocateFromAccount}>{loc.headerButton.account}</button>
          <button onClick={this.signOut}>{loc.headerButton.logOut}</button>
        </header>
        <div className="layout">
          {this.props.children}
        </div>
      </ React.Fragment>
    );
  }
}

export default withRouter(MainLayout);
