import * as React from 'react';

export const Tumbleweed = () => {
  const animate = (options: any) => {
    const start = performance.now();

    requestAnimationFrame(function animate(time: any) {
      // timeFraction от 0 до 1
      let timeFraction = (time - start) / options.duration;
      timeFraction > 1 && (timeFraction = 1);

      // текущее состояние анимации
      const progress = options.timing(timeFraction);
      options.draw(progress);
      timeFraction < 1 && requestAnimationFrame(animate);
    });
  };

  const makeEaseOut = (timing: any) => (timeFraction: any) => 1 - timing(1 - timeFraction);

  const bounce = (timeFraction: any) => {
    for (let i = 0, a = 0, b = 1; i < 4 ; i += 1, a += b, b /= 2) {
      if (timeFraction >= (7 - 4 * a) / 11) {
        return -Math.pow((11 - 6 * a - 11 * timeFraction) / 4, 2) + Math.pow(b, 2);
      }
    }
    return null;
  };

  const quad = (timeFraction: any) => Math.pow(timeFraction, 2);

  const startAnim = () => {
    const ball: any = document.getElementById('ball');
    const field: any = document.getElementById('baner');

    const height = field.clientHeight - ball.clientHeight;
    const width = 1000;

    animate({
      duration: 2000,
      timing: makeEaseOut(bounce),
      draw: (progress: any) => {
        ball.style.top = `${height * progress}px`;
      },
    });

    animate({
      duration: 2000,
      timing: makeEaseOut(quad),
      draw: (progress: any) => {
        ball.style.left = `${width * progress}px`;
      },
    });
  };

  return (
    <div id="baner">
      <span id="ball" onClick={startAnim}></span>
    </div>
  );
};
