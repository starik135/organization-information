import * as React from 'react';
import classnames from 'classnames';
import { Button  } from 'antd';

import { loc } from '../../../localization';
import { randomString } from '../../../helper';
import { OrganizationInfo } from '../../../entities';
import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

class OrganizationsName extends React.Component<AllProps, ComponentState> {

  private renderHeader = () => {
    const {
      newOrganization,
      deleteOrganization,
      currentOrganization,
    } = this.props;

    return (
      <React.Fragment>
        <Button onClick={newOrganization}>
          {loc.buttonName.add}
        </Button>
        <Button onClick={() => deleteOrganization(currentOrganization.id)}>
          {loc.buttonName.delete}
        </Button>
      </React.Fragment>
    );
  }

  private renderOrganizationList = () => {
    const {
      items,
      setOrganization,
      currentOrganization,
    } = this.props;

    return items && items.map((item: OrganizationInfo) => (
      <li
        key={randomString()}
        onClick={() => setOrganization(item.id)}
        className={classnames(
          item.organizationName
            && item.organizationName.length === 0
              ? 'organization-name__list_without-name'
              : 'organization-name__list_organization',
          currentOrganization && item.id === currentOrganization.id && 'select-organization',
        )}
      >
        {item.organizationName}
      </li>
    ));
  }

  render() {
    const { visibilityButton } = this.props;
    return (
      <div className="organization-name">
        {
          visibilityButton
            && <header className="organization-name__header">{this.renderHeader()}</header>
        }
        <ul className="organization-name__list">
          {this.renderOrganizationList()}
        </ul>
      </div>
    );
  }
}

export default OrganizationsName;
