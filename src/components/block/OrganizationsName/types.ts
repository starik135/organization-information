import { OrganizationInfo } from '../../../entities';

export interface ComponentProps {
  items: OrganizationInfo[];
  visibilityButton: boolean;
  currentOrganization: OrganizationInfo;
  setOrganization: (id: number) => void;
  deleteOrganization: (id: number) => void;
  newOrganization: () => void;
}

export interface ComponentState { }

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
