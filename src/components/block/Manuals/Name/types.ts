export interface ComponentProps {
  manualsData: string[];
  setCurrentManual: (value: string) => void;
}

export interface ComponentState {
  selectId: number;
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
