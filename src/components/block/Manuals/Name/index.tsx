import * as React from 'react';
import classnames from 'classnames';

import { loc } from '../../../../localization';
import { randomString } from '../../../../helper';
import { TYPE_ORGANIZATION, USER } from '../../../../constants';

import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

class ManualsName extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      selectId: 0,
    };
  }

  private handlerClick = (item: string, id: number) => {
    const { setCurrentManual } = this.props;

    this.setState({ selectId: id });
    item === loc.manualsName.typeOrganizations && setCurrentManual(TYPE_ORGANIZATION);
    item === loc.manualsName.users && setCurrentManual(USER);
  }

  render() {
    const { manualsData } = this.props;
    const { selectId } = this.state;

    return <div className="manuals-name">
      {
        manualsData.map((item: string, index: number) => (
          <p
            key={randomString()}
            onClick={() => this.handlerClick(item, index)}
            className={classnames({
              'manuals-name__item': true,
              'select-item': index === selectId,
            })}
          >
            {item}
          </p>
        ))
      }
    </div>;
  }
}

export default ManualsName;
