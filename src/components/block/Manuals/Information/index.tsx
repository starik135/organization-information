import * as React from 'react';

import Table from '../../../../common/Table';
import AddManual from '../../../modals/AddManual';

import { tableConfig } from './tableConfig';
import { loc } from '../../../../localization';
import { UserDate } from '../../../../entities';
import { randomString } from '../../../../helper';

import {
  AllProps,
  ComponentState,
} from './types';

import {
  USER,
  USER_FORM,
  TYPE_FORM,
  TYPE_ORGANIZATION,
} from '../../../../constants';

import './styles.scss';

class ManualsInformation extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      selectId: 0,
      visible: false,
    };
  }

  private selectItem = (id: number) => {
    this.setState({ selectId: id });
  }

  private setVisibleModal = () => {
    this.setState({ visible: !this.state.visible });
  }

  private renderButtonBlock = (deleteFunc: (selectId: number) => void, selectId: number) => (
    <div className="manuals-information__buttons-wrapper">
      <button onClick={this.setVisibleModal}>{loc.buttonName.add}</button>
      <button onClick={() => deleteFunc(selectId)}>{loc.buttonName.delete}</button>
    </div>
  )

  private renderTypeOrganizationManualsElements = (selectId: number, dataList: string[]) => (
    <div className="manuals-information__list">
      {
        dataList && dataList.map((item: string, index: number) => (
          <p
            key={randomString()}
            onClick={() => this.selectItem(index)}
            className={index === selectId ? 'select-item' : ''}
          >
            {item}
          </p>
        ))
      }
    </div>
  )

  private renderUsersManualsElements = (dataTable: UserDate[]) => (
    <div className="manuals-information__table">
      <Table
        tableConfig={tableConfig}
        data={dataTable}
        getSelectedId={this.selectItem}
      />
    </div>
  )

  render() {
    const {
      typesData,
      usersData,
      deleteType,
      deleteUser,
      currentManual,
      handleSubmitType,
      handleSubmitUser,
    } = this.props;
    const { selectId, visible } = this.state;

    const isUser = currentManual === USER;
    const isTypeOrganization = currentManual === TYPE_ORGANIZATION;

    return <div className="manuals-information">
      {this.renderButtonBlock(isUser ? deleteUser : deleteType, selectId)}
      {
        isTypeOrganization
          && visible
          && <AddManual
            view={TYPE_FORM}
            setVisible={this.setVisibleModal}
            title={loc.modalTypeManual.addType}
            handleSubmit={handleSubmitType}
          />
      }
      {isTypeOrganization && this.renderTypeOrganizationManualsElements(selectId, typesData)}
      {
        isUser
          && visible
          && <AddManual
            view={USER_FORM}
            setVisible={this.setVisibleModal}
            title={loc.modalUsersManual.addUser}
            handleSubmit={handleSubmitUser}
          />
      }
      {isUser && this.renderUsersManualsElements(usersData)}
    </div>;
  }
}

export default ManualsInformation;
