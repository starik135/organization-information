import { UserDate } from '../../../../entities';

export interface ComponentProps {
  currentManual: string;
  typesData: string[];
  usersData: UserDate[];
  deleteType: (id: number) => void;
  deleteUser: (id: number) => void;
  handleSubmitType: (value: string) => void;
  handleSubmitUser: (value: UserDate) => void;
}

export interface ComponentState {
  selectId: number;
  visible: boolean;
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
