export const tableConfig = [
  { key: 'lastName', name: 'Last name' },
  { key: 'firstName', name: 'First name' },
  { key: 'patronymic', name: 'Patronymic' },
];
