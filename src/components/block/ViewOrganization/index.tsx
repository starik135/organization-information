import * as React from 'react';

import ChatRoom from '../../forms/ChatRoom';
import { randomString } from '../../../helper';
import { OrganizationInfo } from '../../../entities';

import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

const io = require('socket.io-client');
const socket = io.connect('http://localhost:8081');

class ViewOrganization extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      isChatRoom: false,
      currentUserId: props.currentOrganization.id,
    };
  }

  private setChatRoom = () => {
    const { userInfo } = this.props;
    const { isChatRoom, currentUserId } = this.state;

    isChatRoom
      ? socket.emit('unsubscribe', userInfo)
      : socket.emit('subscribe', userInfo, currentUserId);

    this.setState({ isChatRoom: !isChatRoom });
  }

  private renderOrganizationInfo = (currentOrganization: OrganizationInfo) => (
    <div>
      {
        Object
          .keys(currentOrganization)
          .map((item: string) => <p key={randomString()}>{item}: {currentOrganization[item]}</p>)
      }
      <button onClick={this.setChatRoom}>chat with him</button>
    </div>
  )

  render() {
    const { isChatRoom } = this.state;
    const { currentOrganization, userInfo, checkHistory } = this.props;

    return (
      <React.Fragment>
        {
          isChatRoom
            ? <ChatRoom
              checkHistory={checkHistory}
              setChatRoom={this.setChatRoom}
              userInfo={userInfo}
            />
            : this.renderOrganizationInfo(currentOrganization)
        }
      </React.Fragment>
    );
  }
}

export default ViewOrganization;
