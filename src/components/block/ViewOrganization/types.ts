import { OrganizationInfo } from '../../../entities';

export interface ComponentProps {
  userInfo: any;
  currentOrganization: OrganizationInfo;
  checkHistory: (id: string) => void;
}

export interface ComponentState {
  isChatRoom: boolean;
  currentUserId: number;
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
