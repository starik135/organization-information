import * as React from 'react';

import { compose } from 'redux';
import { notification } from 'antd';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { randomString } from '../../../helper';

import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

const io = require('socket.io-client');
const socket = io.connect('http://localhost:8081');

class ChatRoom extends React.Component<AllProps, ComponentState> {
  constructor(props: any) {
    super(props);
    this.state = {
      value: '',
      chatHistory: [],
    };
  }

  componentDidMount() {
    /*socket.on('user:accept', this.userAccept);
    socket.on('user:join', this.userJoin);*/
    socket.on('user left', this.userLeft);
    socket.on('user joined', this.userJoin);
    socket.on('sendMessage', this.messageReceive);
  }

  private userLeft = (value: any) => {
    notification.warning({
      message: 'Warning',
      description: `${value} left chat`,
    });
  }

  private userJoin = (value: any, id: string) => {
    this.props.checkHistory(id);
    notification.success({
      message: 'Succes',
      description: `${value} joined to chat`,
    });
  }

  private messageReceive = (receivedMessage: string) => {
    const { chatHistory } = this.state;

    this.setState({ chatHistory: chatHistory.concat([receivedMessage]) });
  }

  private handleChange = (event: any) => {
    this.setState({ value: event.target.value });
  }

  private handleSubmit = (event: any) => {
    const { userInfo } = this.props;

    event.preventDefault();

    socket.emit('sendMessage', {
      id: `${userInfo.lastName} ${userInfo.firstName} ${userInfo.patronymic}`,
      message: this.state.value,
    });
  }

  private renderChat = (chatHistory: any) => (
    <div>
      <div>
        {
          chatHistory.map((item: any) => (
            <div key={randomString()}>
              <span>
                <p>{item.id}</p>
                <p>{item.time}</p>
              </span>
              <p>{item.message}</p>
            </div>
          ))
        }
      </div>
      <form onSubmit={this.handleSubmit}>
        <input type="text" value={this.state.value} onChange={this.handleChange} />
        <input type="submit" value="Submit" />
      </form>
    </div>
  )

  render() {
    const { chatHistory } = this.state;

    return (
      <div>
        <button onClick={this.props.setChatRoom}>Exite room</button>
        {this.renderChat(chatHistory)}
      </div>
    );
  }
}

const validate = (values: any) => {
  const errors = {};

  return errors;
};

const formSettings = {
  validate,
  form: 'usersChat',
  enableReinitialize: false,
};

const mapStateToProps = (state: any): any => ({});

export default compose<any>(
  connect<any>(mapStateToProps),
  reduxForm(formSettings),
)(ChatRoom);
