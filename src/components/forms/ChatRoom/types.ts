export interface ComponentProps {
  userInfo: any;
  roomId: string;
  setChatRoom: () => void;
  checkHistory: (id: string) => void;
}

export interface ComponentState {
  value: string;
  chatHistory: any[];
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
