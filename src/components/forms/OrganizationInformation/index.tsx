import * as React from 'react';

import { get } from 'lodash';
import { Button } from 'antd';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import Textarea from '../../fields/Textarea';
import DataPicker from '../../fields/DataPicker';
import TypeOfManual from '../../modals/TypeOfManual';
import UsersOfManual from '../../modals/UsersOfManual';
import TextInputField from '../../fields/TextInputField';

import { loc } from '../../../localization';

import {
  normalizePhone,
  normalizeCountEmployees,
} from '../../../helper';

import {
  RootState,
  OrganizationInfo,
  UserDate,
} from '../../../entities';

import {
  AllProps,
  ComponentState,
  MapStateProps,
} from './types';

import {
  NOTES,
  CHIEF_NAME,
  PHONE_NUMBER,
  CURRENT_DATA,
  COUNT_EMPLOYEES,
  FIELD_TYPE_TEXT,
  ORGANIZATION_ID,
  ORGANIZATION_NAME,
  // PHONE_NUMBER_LENGTH,
  ADDRESS_ORGANIZATION,
  TYPE_OF_ORGANIZATION,
  CALENDAR_ORGANIZATION,
  ADDITIONAL_INFORMATION,
  ORGANIZATION_INFORMATION_FORM,
  DEFAULT_ORGANIZATION_FORM,
} from '../../../constants';

import './styles.scss';

class OrganizationInformation extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      visibleTypeModal: false,
      visibleUsersModal: false,
    };
  }

  private visibleTypeModal = () => {
    this.setState({ visibleTypeModal: !this.state.visibleTypeModal });
  }

  private visibleUsersModal = () => {
    this.setState({ visibleUsersModal: !this.state.visibleUsersModal });
  }

  private renderBasicInformation = () => (
    <React.Fragment>
      <Field
        name={ORGANIZATION_ID}
        type={FIELD_TYPE_TEXT}
        component={TextInputField}
        placeholder={loc.organizationInformationForm.id}
        className="organization-information__organization-id hidden"
      />
      <Field
        type={FIELD_TYPE_TEXT}
        name={ORGANIZATION_NAME}
        component={TextInputField}
        placeholder={loc.organizationInformationForm.name}
        className="organization-information__organization-name"
      />
      <div className="organization-information__communication-wrapper">
        <Field
          name={NOTES}
          className="notes"
          component={Textarea}
          label={loc.organizationLabel.description}
          placeholder={loc.organizationInformationForm.description}
        />
        <div className="address-phone-wrapper">
          <Field
            component={TextInputField}
            name={ADDRESS_ORGANIZATION}
            label={loc.organizationLabel.address}
            placeholder={loc.organizationInformationForm.address}
            className="address-phone-wrapper__organization-address"
          />
          <Field
            name={PHONE_NUMBER}
            type={FIELD_TYPE_TEXT}
            component={TextInputField}
            normalize={normalizePhone}
            label={loc.organizationLabel.phone}
            className="address-phone-wrapper__phone-number"
          />
        </div>
      </div>
      <Field
        name={COUNT_EMPLOYEES}
        type={FIELD_TYPE_TEXT}
        component={TextInputField}
        normalize={normalizeCountEmployees}
        label={loc.organizationLabel.countEmployees}
        className="organization-information__count-emploeeys"
      />
      <Field
        component={Textarea}
        type={FIELD_TYPE_TEXT}
        name={ADDITIONAL_INFORMATION}
        label={loc.organizationLabel.additionalInformation}
        className="organization-information__additional-information"
      />
    </React.Fragment>
  )

  private renderTypeOfManual = (
    visible: boolean,
    setOrganization: (data: OrganizationInfo | number) => void,
    currentForm: OrganizationInfo,
    typesData: string[],
  ) => (
      <div className="organization-information__manual-wrapper">
        <Field
          disabled
          type={FIELD_TYPE_TEXT}
          component={TextInputField}
          className="type-of-manual"
          name={TYPE_OF_ORGANIZATION}
          label={loc.organizationLabel.typeOfManual}
        />
        <Button onClick={this.visibleTypeModal}>...</Button>
        {
          visible
            && <TypeOfManual
              data={typesData}
              visible={visible}
              currentForm={currentForm}
              setOrganization={setOrganization}
              visibleModal={this.visibleTypeModal}
            />
        }
      </div>
    )

  private renderUsersOfManual = (
    visible: boolean,
    setOrganization: (data: OrganizationInfo | number) => void,
    currentForm: OrganizationInfo,
    userData: UserDate[],
  ) => (
      <div className="modal">
        <Field
          disabled
          name={CHIEF_NAME}
          type={FIELD_TYPE_TEXT}
          component={TextInputField}
          className="users-of-manual"
          label={loc.organizationLabel.chief}
        />
        <Button onClick={this.visibleUsersModal}>...</Button>
        {
          visible
          && <UsersOfManual
            data={userData}
            visible={visible}
            currentForm={currentForm}
            setOrganization={setOrganization}
            visibleModal={this.visibleUsersModal}
          />
        }
      </div>
    )

  private renderDatePicker = () => (
    <Field
      component={DataPicker}
      id={CALENDAR_ORGANIZATION}
      name={CALENDAR_ORGANIZATION}
      className="date-picker-field"
    />
  )

  private renderSaveZone = (valid: boolean) => (
    <div className="organization-information__button-wrapper">
      <button
        type="submit"
        disabled={!valid}
        className="organization-information__button-wrapper_save-information"
      >
        {loc.organizationInformationForm.saveButton}
      </button>
    </div>
  )

  render() {
    const {
      valid,
      handleSubmit,
      usersData,
      typesData,
      setOrganization,
      currentForm = { ...DEFAULT_ORGANIZATION_FORM, id: 0 },
    } = this.props;
    const { visibleTypeModal, visibleUsersModal } = this.state;

    return (
      <form onSubmit={handleSubmit} className="organization-information">
        {this.renderBasicInformation()}
        <hr className="organization-information__dividing-line" />
        <React.Fragment>
          {this.renderTypeOfManual(visibleTypeModal, setOrganization, currentForm, typesData)}
          <div className="organization-information__manual-wrapper">
            {this.renderUsersOfManual(visibleUsersModal, setOrganization, currentForm, usersData)}
            {this.renderDatePicker()}
          </div>
        </React.Fragment>
        {this.renderSaveZone(valid || true)}
      </form>
    );
  }
}

const validate = (values: OrganizationInfo) => {
  const errors = {};

  /*!values[ORGANIZATION_NAME]
    && (errors[ORGANIZATION_NAME] = loc.organizationInformationForm.validateName);

  !values[ADDRESS_ORGANIZATION]
    && (errors[ADDRESS_ORGANIZATION] = loc.organizationInformationForm.validateAddress);

  !values[PHONE_NUMBER]
    && (errors[PHONE_NUMBER] = loc.organizationInformationForm.validatePhoneNumber);

  values[PHONE_NUMBER]
    && values[PHONE_NUMBER].length < PHONE_NUMBER_LENGTH
    && (errors[PHONE_NUMBER] = loc.organizationInformationForm.validateWriteFullNumber);

  !values[COUNT_EMPLOYEES]
    && (errors[COUNT_EMPLOYEES] = loc.organizationInformationForm.validateCountEmplyees);

  !values[ADDITIONAL_INFORMATION] && (
    errors[ADDITIONAL_INFORMATION] = loc.organizationInformationForm.validateAdditionalInformation
  );*/

  return errors;
};

const formSettings = {
  validate,
  form: ORGANIZATION_INFORMATION_FORM,
  enableReinitialize: true,
};

const mapStateToProps = (state: RootState): MapStateProps => {
  const organizationData = get(state, ['organization', 'selectedOrganization', 'data']);
  const currentForm = get(state, ['form', 'OrganizationInformation', 'values']);

  return {
    currentForm,
    initialValues: {
      [ORGANIZATION_ID]: get(organizationData, 'id') || 0,
      [ORGANIZATION_NAME]: get(organizationData, 'organizationName') || '',
      [NOTES]: get(organizationData, 'notes') || '',
      [ADDRESS_ORGANIZATION]: get(organizationData, 'addressOrganization') || '',
      [PHONE_NUMBER]: get(organizationData, 'phoneNumber') || '',
      [COUNT_EMPLOYEES]: get(organizationData, 'countEmployee') || '',
      [ADDITIONAL_INFORMATION]: get(organizationData, 'additionalInformation') || '',
      [TYPE_OF_ORGANIZATION]: get(organizationData, 'typeOfOrganization') || '',
      [CHIEF_NAME]: get(organizationData, 'chiefName') || '',
      [CALENDAR_ORGANIZATION]: get(organizationData, 'calendarOrganization') || CURRENT_DATA,
    },
  };
};

export default compose<React.ComponentType<AllProps>>(
  connect<MapStateProps>(mapStateToProps),
  reduxForm(formSettings),
)(OrganizationInformation);
