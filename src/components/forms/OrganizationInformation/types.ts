import { OrganizationInfo, UserDate } from '../../../entities';

export interface ComponentProps {
  valid?: boolean;
  typesData: string[];
  usersData: UserDate[];
  onSubmit: (values: OrganizationInfo) => void;
  handleSubmit?: (values: any) => void;
  setOrganization: (data: OrganizationInfo | number) => void;
}

export interface ComponentState {
  visibleTypeModal: boolean;
  visibleUsersModal: boolean;
}

export interface MapStateProps {
  currentForm?: OrganizationInfo;
  initialValues?: OrganizationInfo;
}

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
