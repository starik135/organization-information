import * as React from 'react';

import { Button } from 'antd';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import TextInputField from '../../fields/TextInputField';

import { loc } from '../../../localization';
import { RootState, ValidateUsersManual } from '../../../entities';

import {
  AllProps,
  ComponentState,
  MapStateProps,
} from './types';

import {
  FIELD_TYPE_TEXT,
  SIGN_IN_USER_MAIL,
  SIGN_IN_USER_FORM,
  SIGN_IN_USER_PASSWORD,
  FIELD_TYPE_PASSWORD,
} from '../../../constants';

import './styles.scss';

class SignInForm extends React.Component<AllProps, ComponentState> {
  private renderFieldsZone = () => (
    <React.Fragment>
      <Field
        type={FIELD_TYPE_TEXT}
        id={SIGN_IN_USER_MAIL}
        name={SIGN_IN_USER_MAIL}
        component={TextInputField}
        label={loc.login.email}
        className={'sign-form__login-field'}
      />
      <Field
        password
        type={FIELD_TYPE_PASSWORD}
        id={SIGN_IN_USER_PASSWORD}
        name={SIGN_IN_USER_PASSWORD}
        component={TextInputField}
        label={loc.login.password}
        className={'sign-form__password-field'}
      />
    </React.Fragment>
  )

  private renderSaveZone = (valid: boolean) => (
    <Button htmlType="submit" disabled={!valid}>
      {loc.organizationInformationForm.saveButton}
    </Button>
  )

  render() {
    const { handleSubmit, valid } = this.props;

    return (
      <form onSubmit={handleSubmit} className="sign-form">
        {this.renderFieldsZone()}
        {this.renderSaveZone(valid || true)}
      </form>
    );
  }
}

const validate = (values: ValidateUsersManual) => {
  const errors = {};

 /*!values[SIGN_IN_USER_MAIL]
    && (errors[SIGN_IN_USER_MAIL] = loc.modalUsersManual.validateName);

  !values[SIGN_IN_USER_PASSWORD]
    && (errors[SIGN_IN_USER_PASSWORD] = loc.modalUsersManual.validateLastName);*/

  return errors;
};

const formSettings = {
  validate,
  form: SIGN_IN_USER_FORM,
};

const mapStateToProps = (state: RootState): MapStateProps => ({ });

export default compose<React.ComponentType<AllProps>>(
  connect<MapStateProps>(mapStateToProps),
  reduxForm(formSettings),
)(SignInForm);
