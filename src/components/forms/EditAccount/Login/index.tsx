import * as React from 'react';

import { Button } from 'antd';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import TextInputField from '../../../fields/TextInputField';

import { loc } from '../../../../localization';
import { Card } from '../../../../common/Card';
import { RootState, ValidateUsersManual } from '../../../../entities';

import {
  AllProps,
  ComponentState,
  MapStateProps,
} from './types';

import {
  FIELD_TYPE_TEXT,
  ACCOUNT_LOGIN_MAIL,
  ACCOUNT_LOGIN_FORM,
  FIELD_TYPE_PASSWORD,
  ACCOUNT_LOGIN_PASSWORD,
} from '../../../../constants';

import './styles.scss';

class EditLoginForm extends React.Component<AllProps, ComponentState> {
  private renderEditingLogin = (valid: boolean) => (
    <Card
      height={'fit-content'}
      padding={25}
      borderRadius={10}
      borderColor={'#63c765'}
    >
      <fieldset>
        <legend>Editing login and password</legend>
        <Field
          type={FIELD_TYPE_TEXT}
          id={ACCOUNT_LOGIN_MAIL}
          name={ACCOUNT_LOGIN_MAIL}
          component={TextInputField}
          label={loc.account.email}
          className={''}
        />
        <Field
          type={FIELD_TYPE_PASSWORD}
          id={ACCOUNT_LOGIN_PASSWORD}
          name={ACCOUNT_LOGIN_PASSWORD}
          component={TextInputField}
          label={loc.account.password}
          className={''}
        />
        <div className="button-save">
          <Button htmlType="submit" disabled={!valid}>
            {loc.organizationInformationForm.saveButton}
          </Button>
        </div>
      </fieldset>
    </Card>
  )

  render() {
    const { handleSubmit, valid } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        {this.renderEditingLogin(valid || true)}
      </form>
    );
  }
}

const validate = (values: ValidateUsersManual) => {
  const errors = {};

  /*!values[ACCOUNT_LOGIN_MAIL]
    && (errors[ACCOUNT_LOGIN_MAIL] = loc.modalUsersManual.validateName);

  !values[ACCOUNT_LOGIN_PASSWORD]
    && (errors[ACCOUNT_LOGIN_PASSWORD] = loc.modalUsersManual.validateLastName);*/

  return errors;
};

const formSettings = {
  validate,
  form: ACCOUNT_LOGIN_FORM,
};

const mapStateToProps = (state: RootState): MapStateProps => ({ });

export default compose<React.ComponentType<AllProps>>(
  connect<MapStateProps>(mapStateToProps),
  reduxForm(formSettings),
)(EditLoginForm);
