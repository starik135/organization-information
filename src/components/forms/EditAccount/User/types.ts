export interface ComponentProps {
  valid?: boolean;
  handleSubmit: (data: any) => void;
}

export interface ComponentState { }

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
