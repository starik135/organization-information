import * as React from 'react';

import { get } from 'lodash';
import { Button } from 'antd';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import TextInputField from '../../../fields/TextInputField';

import { loc } from '../../../../localization';
import { Card } from '../../../../common/Card';
import { RootState, ValidateUsersManual } from '../../../../entities';

import {
  AllProps,
  ComponentState,
  MapStateProps,
} from './types';

import {
  FIELD_TYPE_TEXT,
  ACCOUNT_USER_FORM,
  ACCOUNT_USER_FIRST_NAME,
  ACCOUNT_USER_LAST_NAME,
  ACCOUNT_USER_PATRONYMIC,
} from '../../../../constants';

import './styles.scss';

class EditUserForm extends React.Component<AllProps, ComponentState> {
  private renderEditingUser = (valid: boolean) => (
    <Card
      height={'fit-content'}
      padding={25}
      borderRadius={10}
      borderColor={'#63c765'}
    >
      <fieldset>
        <legend>Editing user data</legend>
        <Field
          type={FIELD_TYPE_TEXT}
          id={ACCOUNT_USER_FIRST_NAME}
          name={ACCOUNT_USER_FIRST_NAME}
          component={TextInputField}
          label={loc.account.firstName}
          className={''}
        />
        <Field
          type={FIELD_TYPE_TEXT}
          id={ACCOUNT_USER_LAST_NAME}
          name={ACCOUNT_USER_LAST_NAME}
          component={TextInputField}
          label={loc.account.lastName}
          className={''}
        />
        <Field
          type={FIELD_TYPE_TEXT}
          id={ACCOUNT_USER_PATRONYMIC}
          name={ACCOUNT_USER_PATRONYMIC}
          component={TextInputField}
          label={loc.account.patronymic}
          className={''}
        />
        <div className="button-save">
          <Button htmlType="submit" disabled={!valid}>
            {loc.organizationInformationForm.saveButton}
          </Button>
        </div>
      </fieldset>
    </Card>
  )

  render() {
    const { handleSubmit, valid } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        {this.renderEditingUser(valid || true)}
      </form>
    );
  }
}

const validate = (values: ValidateUsersManual) => {
  const errors = {};

  return errors;
};

const formSettings = {
  validate,
  form: ACCOUNT_USER_FORM,
};

const mapStateToProps = (state: RootState): MapStateProps => {
  const value = get(state, ['user', 'accountData', 'data']);

  return {
    value,
    initialValues: {
      [ACCOUNT_USER_FIRST_NAME]: value.firstName || '',
      [ACCOUNT_USER_LAST_NAME]: value.lastName || '',
      [ACCOUNT_USER_PATRONYMIC]: value.patronymic || '',
    },
  };
};
export default compose<React.ComponentType<AllProps>>(
  connect<MapStateProps>(mapStateToProps),
  reduxForm(formSettings),
)(EditUserForm);
