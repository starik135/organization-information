import * as React from 'react';

import { get } from 'lodash';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import TextInputField from '../../fields/TextInputField';

import { loc } from '../../../localization';
import { RootState, ValidateUsersManual } from '../../../entities';

import {
  AllProps,
  ComponentState,
  MapStateProps,
} from './types';

import {
  ADD_USER_NAME,
  ADD_USER_FORM,
  FIELD_TYPE_TEXT,
  ADD_USER_LAST_NAME,
  ADD_USER_PATRONYMIC,
} from '../../../constants';

import './styles.scss';

class AddUserForm extends React.Component<AllProps, ComponentState> {

  private renderFieldsZone = () => (
    <React.Fragment>
      <Field
        type={FIELD_TYPE_TEXT}
        id={ADD_USER_NAME}
        name={ADD_USER_NAME}
        component={TextInputField}
        label={loc.modalUsersManual.name}
        className={'add-user-form__name-field'}
      />
      <Field
        type={FIELD_TYPE_TEXT}
        id={ADD_USER_LAST_NAME}
        name={ADD_USER_LAST_NAME}
        component={TextInputField}
        label={loc.modalUsersManual.lastName}
        className={'add-user-form__last-name-field'}
      />
      <Field
        type={FIELD_TYPE_TEXT}
        id={ADD_USER_PATRONYMIC}
        name={ADD_USER_PATRONYMIC}
        component={TextInputField}
        label={loc.modalUsersManual.patronymic}
        className={'add-user-form__patronymic-field'}
      />
    </React.Fragment>
  )

  private renderSaveZone = (valid: boolean) => (
    <div className="add-user-form__save-button">
      <button
        type="submit"
        disabled={!valid}
      >
        {loc.organizationInformationForm.saveButton}
      </button>
    </div>
  )

  render() {
    const { handleSubmit, valid } = this.props;

    return (
      <form
        onSubmit={handleSubmit}
        className="add-user-form"
      >
        {this.renderFieldsZone()}
        {this.renderSaveZone(valid || true)}
      </form>
    );
  }
}

const validate = (values: ValidateUsersManual) => {
  const errors = {};

  /*!values[ADD_USER_NAME]
    && (errors[ADD_USER_NAME] = loc.modalUsersManual.validateName);

  !values[ADD_USER_LAST_NAME]
    && (errors[ADD_USER_LAST_NAME] = loc.modalUsersManual.validateLastName);

  !values[ADD_USER_PATRONYMIC]
    && (errors[ADD_USER_PATRONYMIC] = loc.modalUsersManual.validatePatronymic);*/

  return errors;
};

const formSettings = {
  validate,
  form: ADD_USER_FORM,
  enableReinitialize: true,
};

const mapStateToProps = (state: RootState): MapStateProps => {
  const manualData = get(state, ['manual', 'user', 'data']);

  return {
    initialValues: {
      [ADD_USER_NAME]: manualData && manualData.name || '',
      [ADD_USER_LAST_NAME]: manualData && manualData.lastName || '',
      [ADD_USER_PATRONYMIC]: manualData && manualData.patronymic || '',
    },
  };
};

export default compose<React.ComponentType<AllProps>>(
  connect<MapStateProps>(mapStateToProps),
  reduxForm(formSettings),
)(AddUserForm);
