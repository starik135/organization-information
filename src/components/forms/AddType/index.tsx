import * as React from 'react';

import { get } from 'lodash';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import TextInputField from '../../fields/TextInputField';

import { loc } from '../../../localization';
import { RootState, ValidateTypesManual } from '../../../entities';

import {
  AllProps,
  ComponentState,
  MapStateProps,
} from './types';

import {
  ADD_TYPE,
  ADD_TYPE_FORM,
  FIELD_TYPE_TEXT,
} from '../../../constants';

import './styles.scss';

class AddType extends React.Component<AllProps, ComponentState> {
  private renderFieldsZone = () => (
    <Field
      id={ADD_TYPE}
      name={ADD_TYPE}
      type={FIELD_TYPE_TEXT}
      component={TextInputField}
      label={loc.modalTypeManual.type}
      className={'add-type-form__type-field'}
    />
  )

  private renderSaveZone = (valid: boolean) => (
    <div className="add-type-form__save-button">
      <button type="submit" disabled={!valid}>
        {loc.organizationInformationForm.saveButton}
      </button>
    </div>
  )

  render() {
    const { handleSubmit, valid } = this.props;

    return (
      <form onSubmit={handleSubmit} className="add-type-form">
        {this.renderFieldsZone()}
        {this.renderSaveZone(valid || true)}
      </form>
    );
  }
}

const validate = (values: ValidateTypesManual) => {
  const errors = {};

  !values[ADD_TYPE] && (errors[ADD_TYPE] = loc.modalTypeManual.validateType);

  return errors;
};

const formSettings = {
  validate,
  form: ADD_TYPE_FORM,
  enableReinitialize: true,
};

const mapStateToProps = (state: RootState): MapStateProps => {
  const manualData = get(state, ['manual', 'type', 'data']);

  return {
    initialValues: {
      [ADD_TYPE]: manualData && manualData.value || '',
    },
  };
};

export default compose<React.ComponentType<AllProps>>(
  connect<MapStateProps>(mapStateToProps),
  reduxForm(formSettings),
)(AddType);
