import * as React from 'react';
import { WrappedFieldProps } from 'redux-form';

import { TextInputControlProps } from '../../controls/TextInputControl';

import './styles.scss';

interface TextInputProps extends TextInputControlProps {
  className?: string;
  label: string;
  type: string;
}
interface TextInputState {}

type AllTextInputProps = TextInputProps & WrappedFieldProps & any;

class Textarea extends React.Component<AllTextInputProps, TextInputState> {
  render() {
    const { input, label, type, meta, className } = this.props;

    return (
      <div className={className}>
        {label && <label>{label}</label>}
        <div className="warning-wrapper">
          <textarea {...input} placeholder={label} type={type} />
          {
            meta.touched
              && ((meta.error && <span>{meta.error}</span>)
              || (meta.warning && <span>{meta.warning}</span>))
          }
        </div>
      </div>
    );
  }
}

export default Textarea;
