import * as React from 'react';

import { Input } from 'antd';
import { WrappedFieldProps } from 'redux-form';

import { TextInputControlProps } from '../../controls/TextInputControl';

import './styles.scss';

interface TextInputProps extends TextInputControlProps {
  className?: string;
  label: string;
  type: string;
  password: boolean;
  disabled: boolean;
}
interface TextInputState {}

type AllTextInputProps = TextInputProps & WrappedFieldProps & any;

class TextInputField extends React.Component<AllTextInputProps, TextInputState> {
  render() {
    const { input, label, type, meta, className, disabled, password } = this.props;

    return (
      <div className={className}>
        {label && <label>{label}</label>}
        <div className="warning-wrapper">
          {
            password
              ? <Input.Password {...input} type={type} disabled={disabled} />
              : <Input {...input} type={type} disabled={disabled} />
          }
          {
            meta.touched
              && ((meta.error && <span>{meta.error}</span>)
              || (meta.warning && <span>{meta.warning}</span>))
          }
        </div>
      </div>
    );
  }
}

export default TextInputField;
