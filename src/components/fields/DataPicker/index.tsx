import * as React from 'react';
import * as moment from 'moment';

import DatePicker from 'react-datepicker';
import { WrappedFieldProps } from 'redux-form';

import { TextInputControlProps } from '../../controls/TextInputControl';

import './styles.scss';
import 'react-datepicker/dist/react-datepicker.css';

interface TextInputProps extends TextInputControlProps {
  className?: string;
  label: string;
  type: string;
}
interface TextInputState {
  date: Date;
}

type AllTextInputProps = TextInputProps & WrappedFieldProps & any;

class DataPicker extends React.Component<AllTextInputProps, TextInputState> {
  render() {
    const { input, label, meta, className } = this.props;

    const onChange = (event: any) => {
      input.onChange(event);
    };

    return (
      <div className={className}>
        {label && <label>{label}</label>}
        <div className="warning-wrapper">
          <DatePicker
            selected={input.value && moment(input.value).toDate()}
            onChange={onChange}
          />
          {
            meta.touched
              && ((meta.error && <span>{meta.error}</span>)
              || (meta.warning && <span>{meta.warning}</span>))
          }
        </div>
      </div>
    );
  }
}

export default DataPicker;
