import * as React from 'react';

import { Modal } from 'antd';

import AddType from '../../forms/AddType';
import AddUserForm from '../../forms/AddUser';

import {
  AllProps,
  ComponentState,
} from './types';

import {
  TYPE_FORM,
  USER_FORM,
} from '../../../constants';

import './styles.scss';

class AddManual extends React.Component<AllProps, ComponentState> {
  render() {
    const { view, setVisible, title, handleSubmit } = this.props;

    return <Modal
      visible
      title={title}
      footer={false}
      onOk={setVisible}
      onCancel={setVisible}
    >
      {view === TYPE_FORM && <AddType handleSubmit={handleSubmit} />}
      {view === USER_FORM && <AddUserForm handleSubmit={handleSubmit} />}
    </Modal>;
  }
}

export default AddManual;
