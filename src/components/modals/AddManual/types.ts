
export interface ComponentProps {
  view: string;
  title: string;
  setVisible: () => void;
  handleSubmit: (value: any) => void;
}

export interface ComponentState { }

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
