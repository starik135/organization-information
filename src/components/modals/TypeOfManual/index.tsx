import * as React from 'react';

import { Modal, Input } from 'antd';

import { LARGE } from '../../../constants';
import { loc } from '../../../localization';
import { randomString, filterList } from '../../../helper';

import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

const Search = Input.Search;

class TypeOfManual extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      dataList: this.props.data,
      selectId: 0,
    };
  }

  private selectItem = (id: number) => {
    this.setState({ selectId: id });
  }

  private searchValue = (value: string) => {
    this.setState({ dataList: filterList(this.props.data, value) });
  }

  private setData = () => {
    const { setOrganization, currentForm } = this.props;
    const { selectId, dataList } = this.state;

    setOrganization({ ...currentForm, typeOfOrganization: dataList[selectId] });
  }

  render() {
    const { visibleModal, visible } = this.props;
    const { selectId, dataList } = this.state;

    return <Modal
      title={loc.modalTypeManual.title}
      visible={visible}
      onOk={visibleModal}
      onCancel={visibleModal}
      footer={false}
      className="type-modal-form"
    >
      <Search
        placeholder={loc.modalTypeManual.inputPlaceholder}
        enterButton={loc.modalTypeManual.enterButton}
        onSearch={this.searchValue}
        size={LARGE}
      />
      <div className="type-modal-form__list">
        {
          dataList && dataList.map((item: string, index: number) => (
            <p
              key={randomString()}
              className={selectId === index ? 'select-item' : ''}
              onClick={() => this.selectItem(index)}
            >
              {item}
            </p>
          ))
        }
      </div>
      <div className="type-modal-form__save-button">
        <button onClick={this.setData}>{loc.modalTypeManual.selectButton}</button>
      </div>
    </Modal>;
  }
}

export default TypeOfManual;
