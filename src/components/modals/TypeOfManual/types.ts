import { OrganizationInfo } from '../../../entities';

export interface ComponentProps {
  visible: boolean;
  currentForm: OrganizationInfo;
  data: string[];
  visibleModal: () => void;
  setOrganization: (data: OrganizationInfo) => void;
}

export interface ComponentState {
  selectId: number;
  dataList: string[];
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
