import * as React from 'react';

// import { isEqual } from 'lodash';
import { Modal, Input } from 'antd';

import Table from '../../../common/Table';
import { LARGE } from '../../../constants';
import { loc } from '../../../localization';
import { tableConfig } from './config';
import { findValue } from '../../../helper';

import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

const Search = Input.Search;

class UsersOfManual extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      dataTable: this.props.data,
      rowId: 0,
    };
  }

  private searchValue = (value: string) => {
    this.setState({ dataTable: findValue(this.props.data, value) });
  }

  private getSelectedRow = (id: number) => {
    this.setState({ rowId: id });
  }

  private setData = () => {
    const { setOrganization, currentForm } = this.props;
    const { rowId, dataTable } = this.state;

    const item = dataTable[rowId];
    const value = `${item.lastName} ${item.firstName} ${item.patronymic}`;

    setOrganization({ ...currentForm, chiefName: value });
  }

  render() {
    const { visibleModal, visible } = this.props;
    const { dataTable } = this.state;

    return <Modal
      title={loc.modalTypeManual.title}
      visible={visible}
      onOk={visibleModal}
      onCancel={visibleModal}
      footer={false}
      className="type-modal-form"
    >
      <Search
        placeholder={loc.modalTypeManual.inputPlaceholder}
        enterButton={loc.modalTypeManual.enterButton}
        onSearch={this.searchValue}
        size={LARGE}
      />
      <Table
        className={'type-modal-form__table'}
        tableConfig={tableConfig}
        data={dataTable}
        getSelectedId={this.getSelectedRow}
      />
      <div className="type-modal-form__save-button">
        <button onClick={this.setData}>{loc.modalTypeManual.selectButton}</button>
      </div>
    </Modal>;
  }
}

export default UsersOfManual;
