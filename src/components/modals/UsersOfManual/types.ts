import { OrganizationInfo, UserDate } from '../../../entities';

export interface ComponentProps {
  visible: boolean;
  data: UserDate[];
  currentForm: OrganizationInfo;
  visibleModal: () => void;
  setOrganization: (data: OrganizationInfo) => void;
}

export interface ComponentState {
  dataTable: UserDate[];
  rowId: number;
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
