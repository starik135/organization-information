import * as React from 'react';

import { get } from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { compose, Dispatch, AnyAction } from 'redux';

import MainLayout from '../../layouts/MainLayout';
import ViewOrganization from '../../block/ViewOrganization';
import OrganizationsName from '../../block/OrganizationsName';
import OrganizationInformation from '../../forms/OrganizationInformation';

import { isUserAuth } from '../../../helper';

import {
  AllProps,
  MapStateProps,
  ComponentState,
  MapDispatchProps,
} from './types';

import {
  RootState,
  OrganizationInfo,
} from '../../../entities';

import {
  changeAccountMode,
  getAccountRequest,
} from '../../../actions/users';

import {
  getTypeManualRequest,
  getUserManualRequest,
} from '../../../actions/manual';

import {
  newOrganizationRequest,
  sendOrganizationRequest,
  getOrganizationsRequest,
  deleteOrganizationRequest,
  setSelectedOrganizationRequest,
} from '../../../actions/organizations';

import {
  checkHistoryRequest,
} from '../../../actions/chat/checkHistory';

import { CLEAR_USER_DATA } from '../../../constants';

import './styles.scss';

class StartPage extends React.Component<AllProps, ComponentState> {
  componentDidMount() {
    const {
      onGetData,
      onGetUserData,
      onGetTypeData,
      onSetOrganization,
      onGetAccountData,
    } = this.props;

    isUserAuth(this.props.history);
    onGetUserData();
    onGetTypeData();
    onGetData();
    onSetOrganization(0);
    onGetAccountData();
  }

  private handleSubmit = (values: OrganizationInfo) => {
    this.props.onSendData(values);
  }

  public render(): JSX.Element {
    const {
      onLogOut,
      usersData,
      typesData,
      onEditMode,
      isEditMode,
      userInfo,
      onCheckHistory,
      organizations,
      newOrganization,
      onSetOrganization,
      onDeleteOrganization,
      currentOrganization,
    } = this.props;

    return (
      <MainLayout
        isVisibleBookmarks
        isVisibleSwitch
        isEditMode={isEditMode}
        logOut={onLogOut}
        onEditMode={onEditMode}
      >
        <OrganizationsName
          items={organizations}
          visibilityButton={isEditMode}
          newOrganization={newOrganization}
          setOrganization={onSetOrganization}
          deleteOrganization={onDeleteOrganization}
          currentOrganization={currentOrganization}
        />
        {
          isEditMode
            ? <OrganizationInformation
              usersData={usersData}
              typesData={typesData}
              onSubmit={this.handleSubmit}
              setOrganization={onSetOrganization}
            />
            : <ViewOrganization
              userInfo={userInfo}
              checkHistory={onCheckHistory}
              currentOrganization={currentOrganization}
            />
        }
      </MainLayout>
    );
  }
}

const mapStateToProps = (state: RootState): MapStateProps => ({
  typesData: get(state, ['manual', 'type', 'data']),
  usersData: get(state, ['manual', 'users', 'data']),
  organizations: get(state, ['organization', 'organizationInformation', 'data']),
  currentOrganization: get(state, ['organization', 'selectedOrganization', 'data']),
  isEditMode: get(state, ['user', 'editMode', 'data']),
  userInfo: get(state, ['user', 'accountData', 'data']),
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>): MapDispatchProps => ({
  onSetOrganization: (id: OrganizationInfo | number) => {
    dispatch(setSelectedOrganizationRequest(id));
  },
  onGetData: () => dispatch(getOrganizationsRequest()),
  onGetUserData: () => dispatch(getUserManualRequest()),
  onGetTypeData: () => dispatch(getTypeManualRequest()),
  newOrganization: () => dispatch(newOrganizationRequest()),
  onDeleteOrganization: (id: number) => dispatch(deleteOrganizationRequest(id)),
  onSendData: (data: OrganizationInfo) => dispatch(sendOrganizationRequest(data)),
  onLogOut: () => dispatch({ type: CLEAR_USER_DATA }),
  onEditMode: (flag: boolean) => dispatch(changeAccountMode(flag)),
  onGetAccountData: () => dispatch(getAccountRequest()),
  onCheckHistory: (id: string) => dispatch(checkHistoryRequest()),
});

export default compose<React.ComponentType>(
  withRouter,
  connect<MapStateProps, MapDispatchProps>(mapStateToProps, mapDispatchToProps),
)(StartPage);
