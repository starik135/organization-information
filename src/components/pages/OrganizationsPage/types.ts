import { RouteComponentProps } from 'react-router-dom';

import { OrganizationInfo, UserDate } from '../../../entities';

export interface ComponentProps { }

export interface ComponentState { }

export interface MapStateProps {
  usersData: UserDate[];
  typesData: string[];
  organizations: OrganizationInfo[];
  currentOrganization: OrganizationInfo;
  isEditMode: boolean;
  userInfo: any;
}

export interface MapDispatchProps {
  onGetData: () => void;
  onGetTypeData: () => void;
  onGetUserData: () => void;
  newOrganization: () => void;
  onDeleteOrganization: (id: number) => void;
  onSendData: (data: OrganizationInfo) => void;
  onSetOrganization: (id: OrganizationInfo | number) => void;
  onLogOut: () => void;
  onEditMode: (flag: boolean) => void;
  onGetAccountData: () => void;
  onCheckHistory: (id: string) => void;
}

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps & RouteComponentProps<{}>;
