import { RouteComponentProps } from 'react-router-dom';

export interface ComponentProps {
  handleSubmit: () => void;
}

export interface ComponentState { }

export interface MapStateProps { }

export interface MapDispatchProps {
  onChangeLogin: () => void;
  onChangeUser: () => void;
}

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps & RouteComponentProps<{}>;
