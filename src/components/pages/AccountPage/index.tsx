import * as React from 'react';

import { Button, Icon } from 'antd';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Dispatch, AnyAction, compose } from 'redux';

import EditLoginForm from '../../forms/EditAccount/Login';
import EditUserForm from '../../forms/EditAccount/User';
import MainLayout from '../../layouts/MainLayout';

import { loc } from '../../../localization';
import { isUserAuth } from '../../../helper';
import { RootState } from '../../../entities';
import { ORGANIZATIONS_APP_ENDPOINT } from '../../../constants';
import { changeUserRequest, changeLoginRequest } from '../../../actions/users';

import {
  AllProps,
  MapStateProps,
  ComponentState,
  MapDispatchProps,
} from './types';

import './styles';

class AccountPage extends React.Component<AllProps, ComponentState> {
  componentDidMount() {
    isUserAuth(this.props.history);
  }

  private loginHandleSubmit = (event: any) => {
    event.preventDefault();
    this.props.onChangeLogin();
  }

  private userHandleSubmit = (event: any) => {
    event.preventDefault();
    this.props.onChangeUser();
  }
  private changePage = () => this.props.history.push(ORGANIZATIONS_APP_ENDPOINT);

  render() {
    return (
      <MainLayout isVisibleBookmarks={false} isVisibleSwitch={false}>
        <div className="account-page">
          <Button type="primary" onClick={this.changePage}>
            {loc.organizationInformationForm.page}
            <Icon type="right" />
          </Button>
          <div className="account-form">
            <EditLoginForm handleSubmit={this.loginHandleSubmit} />
            <EditUserForm handleSubmit={this.userHandleSubmit} />
          </div>
        </div>
      </MainLayout>
    );
  }
}
const mapStateToProps = (state: RootState): MapStateProps => ({ });

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>): MapDispatchProps => ({
  onChangeLogin: () => dispatch(changeLoginRequest()),
  onChangeUser: () => dispatch(changeUserRequest()),
});

export default compose<React.ComponentType>(
  withRouter,
  connect<MapStateProps, MapDispatchProps>(mapStateToProps, mapDispatchToProps),
)(AccountPage);
