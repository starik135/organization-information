import * as React from 'react';

import { get } from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { compose, Dispatch, AnyAction } from 'redux';

import MainLayout from '../../../components/layouts/MainLayout';
import ManualsName from '../../../components/block/Manuals/Name';
import ManualsInformation from '../../../components/block/Manuals/Information';

import {
  getTypeManualRequest,
  getUserManualRequest,
  sendUserManualRequest,
  sendTypeManualRequest,
  deleteTypeManualRequest,
  deleteUserManualRequest,
} from '../../../actions/manual';

import {
  changeAccountMode,
} from '../../../actions/users';

import { manualsData } from './config';
import { isUserAuth } from '../../../helper';
import { RootState } from '../../../entities';
import { TYPE_ORGANIZATION, CLEAR_USER_DATA } from '../../../constants';

import {
  AllProps,
  MapStateProps,
  ComponentState,
  MapDispatchProps,
} from './types';

class ManualsPage extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      currentManual: TYPE_ORGANIZATION,
    };
  }

  componentDidMount() {
    isUserAuth(this.props.history);

    this.props.getTypeData();
    this.props.getUserData();
  }

  private setCurrentManual = (value: string) => {
    this.setState({ currentManual: value });
  }

  private handleSubmitType = (values: any) => {
    values.preventDefault();
    this.props.onSendTypeData();
  }

  private handleSubmitUser = (values: any) => {
    values.preventDefault();
    this.props.onSendUserData();
  }

  render() {
    const { currentManual } = this.state;
    const {
      typesData,
      usersData,
      deleteTypeData,
      deleteUserData,
      onLogOut,
      onEditMode,
      isEditMode,
    } = this.props;

    return (
      <MainLayout
        isVisibleSwitch
        logOut={onLogOut}
        isVisibleBookmarks
        isEditMode={isEditMode}
        onEditMode={onEditMode}
      >
        <ManualsName
          manualsData={manualsData}
          setCurrentManual={this.setCurrentManual}
        />
        <ManualsInformation
          currentManual={currentManual}
          handleSubmitType={this.handleSubmitType}
          handleSubmitUser={this.handleSubmitUser}
          deleteType={deleteTypeData}
          deleteUser={deleteUserData}
          typesData={typesData}
          usersData={usersData}
        />
      </MainLayout>
    );
  }
}

const mapStateToProps = (state: RootState): MapStateProps => ({
  typesData: get(state, ['manual', 'type', 'data']),
  usersData: get(state, ['manual', 'users', 'data']),
  isEditMode: get(state, ['user', 'editMode', 'data']),
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>): MapDispatchProps => ({
  getTypeData: () => dispatch(getTypeManualRequest()),
  getUserData: () => dispatch(getUserManualRequest()),
  onSendTypeData: () => dispatch(sendTypeManualRequest()),
  onSendUserData: () => dispatch(sendUserManualRequest()),
  deleteTypeData: (id: number) => dispatch(deleteTypeManualRequest(id)),
  deleteUserData: (id: number) => dispatch(deleteUserManualRequest(id)),
  onLogOut: () => dispatch({ type: CLEAR_USER_DATA }),
  onEditMode: (flag: boolean) => dispatch(changeAccountMode(flag)),
});

export default compose<React.ComponentType>(
  withRouter,
  connect<MapStateProps, MapDispatchProps>(mapStateToProps, mapDispatchToProps),
)(ManualsPage);
