import { RouteComponentProps } from 'react-router-dom';

import { UserDate } from '../../../entities';

export interface ComponentProps { }

export interface ComponentState {
  currentManual: string;
}

export interface MapStateProps {
  typesData: string[];
  usersData: UserDate[];
  isEditMode: boolean;
}

export interface MapDispatchProps {
  getTypeData: () => void;
  getUserData: () => void;
  onSendTypeData: () => void;
  onSendUserData: () => void;
  deleteTypeData: (id: number) => void;
  deleteUserData: (id: number) => void;
  onLogOut: () => void;
  onEditMode: (flag: boolean) => void;
}

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps & RouteComponentProps<{}>;
