import { loc } from '../../../localization';

export const manualsData = [
  loc.manualsName.typeOrganizations,
  loc.manualsName.users,
];
