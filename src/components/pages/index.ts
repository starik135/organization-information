import OrganizationsPage from './OrganizationsPage';
import ManualsPage from './ManualsPage';
import SignPage from './SignPage';
import AccountPage from './AccountPage';

export {
  OrganizationsPage,
  ManualsPage,
  SignPage,
  AccountPage,
};
