import { RouteComponentProps } from 'react-router-dom';

export interface ComponentProps { }

export interface ComponentState {
  isSignInForm: boolean;
}

export interface MapStateProps { }

export interface MapDispatchProps {
  onCheckUser: (history: any) => void;
  onRegisterUser: (history: any) => void;
}

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps & RouteComponentProps<{}>;
