import * as React from 'react';

import { Button, Icon } from 'antd';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Dispatch, AnyAction, compose } from 'redux';

import SignInForm from '../../forms/SignIn';
import SignUpForm from '../../forms/SignUp';

import { loc } from '../../../localization';
import { isUserAuth } from '../../../helper';
import { RootState } from '../../../entities';
import { currentUserRequest, registerUserRequest } from '../../../actions/users';

import {
  AllProps,
  MapStateProps,
  ComponentState,
  MapDispatchProps,
} from './types';

import './styles';

class SignInPage extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      isSignInForm: true,
    };
  }

  componentDidMount() {
    isUserAuth(this.props.history);
  }

  private onSignForm = (event: any) => {
    const { isSignInForm } = this.state;
    const { history, onCheckUser, onRegisterUser } = this.props;

    event.preventDefault();
    isSignInForm ? onCheckUser(history) : onRegisterUser(history);
  }

  private changeForm = () => this.setState({ isSignInForm: !this.state.isSignInForm });

  private renderSignUpButton = (isSignInForm: boolean) => (
    <div className="sign-link">
      <Button type="primary" onClick={this.changeForm}>
        {isSignInForm ? loc.register.signUp : loc.login.signIn}
        <Icon type="right" />
      </Button>
    </div>
  )

  render() {
    const { isSignInForm } = this.state;

    return (
      <div className="sign-page">
        <div className="animation-wrapper">
          <p className="animation-wrapper__tumbleweed"></p>
        </div>
        <div className="sign-page__form-wrapper">
          {this.renderSignUpButton(isSignInForm)}
          {
            isSignInForm
              ? <SignInForm handleSubmit={this.onSignForm} />
              : <SignUpForm handleSubmit={this.onSignForm} />
          }
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: RootState): MapStateProps => ({ });

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>): MapDispatchProps => ({
  onCheckUser: (history: any) => dispatch(currentUserRequest(history)),
  onRegisterUser: (history: any) => dispatch(registerUserRequest(history)),
});

export default compose<React.ComponentType>(
  withRouter,
  connect<MapStateProps, MapDispatchProps>(mapStateToProps, mapDispatchToProps),
)(SignInPage);
