import * as React from 'react';
import { Input } from 'antd';

import './styles.scss';

export interface TextInputControlProps {}
interface TextInputControlState {}

class TextInputControl
  extends React.Component<TextInputControlProps, TextInputControlState> {
  render() {
    return (
      <Input
        className="text-input-control"
        {...this.props}
      />
    );
  }
}

export default TextInputControl;
