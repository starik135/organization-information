import { createActionObject, ErrorProps } from '../../actions';

export const REGISTER_USER_REQUEST = '@/REGISTER_USER_REQUEST';
export const REGISTER_USER_FAIL = '@/REGISTER_USER_FAIL';

export const registerUserRequest = (payload: any) => {
  return createActionObject(REGISTER_USER_REQUEST, { payload });
};

export const registerUserFail = (error: ErrorProps) => {
  return createActionObject(REGISTER_USER_FAIL, { ...error });
};
