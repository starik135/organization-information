import { createActionObject, ErrorProps } from '../../actions';

export const CURRENT_USER_REQUEST = '@/CURRENT_USER_REQUEST';
export const CURRENT_USER_RESPONSE = '@/CURRENT_USER_RESPONSE';
export const CURRENT_USER_FAIL = '@/CURRENT_USER_FAIL';

export const currentUserRequest = (payload: any) => {
  return createActionObject(CURRENT_USER_REQUEST, { payload });
};

export const currentUserResponse = (payload: any) => {
  return createActionObject(CURRENT_USER_RESPONSE, { payload });
};

export const currentUserFail = (error: ErrorProps) => {
  return createActionObject(CURRENT_USER_FAIL, { ...error });
};
