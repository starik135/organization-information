import { createActionObject, ErrorProps } from '../../actions';

export const GET_ACCOUNT_REQUEST = '@/GET_ACCOUNT_REQUEST';
export const GET_ACCOUNT_RESPONSE = '@/GET_ACCOUNT_RESPONSE';
export const GET_ACCOUNT_FAIL = '@/GET_ACCOUNT_FAIL';

export const getAccountRequest = () => {
  return createActionObject(GET_ACCOUNT_REQUEST);
};

export const getAccountResponse = (payload: any) => {
  return createActionObject(GET_ACCOUNT_RESPONSE, { payload });
};

export const getAccountFail = (error: ErrorProps) => {
  return createActionObject(GET_ACCOUNT_FAIL, { ...error });
};
