import { createActionObject, ErrorProps } from '../../actions';

export const CHANGE_LOGIN_REQUEST = '@/CHANGE_LOGIN_REQUEST';
export const CHANGE_LOGIN_RESPONSE = '@/CHANGE_LOGIN_RESPONSE';
export const CHANGE_LOGIN_FAIL = '@/CHANGE_LOGIN_FAIL';

export const changeLoginRequest = () => {
  return createActionObject(CHANGE_LOGIN_REQUEST);
};

export const changeLoginResponse = (payload: any) => {
  return createActionObject(CHANGE_LOGIN_RESPONSE, { payload });
};

export const changeLoginFail = (error: ErrorProps) => {
  return createActionObject(CHANGE_LOGIN_FAIL, { ...error });
};
