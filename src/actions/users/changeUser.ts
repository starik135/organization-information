import { createActionObject, ErrorProps } from '../../actions';

export const CHANGE_USER_REQUEST = '@/CHANGE_USER_REQUEST';
export const CHANGE_USER_FAIL = '@/CHANGE_USER_FAIL';

export const changeUserRequest = () => {
  return createActionObject(CHANGE_USER_REQUEST);
};

export const changeUserFail = (error: ErrorProps) => {
  return createActionObject(CHANGE_USER_FAIL, { ...error });
};
