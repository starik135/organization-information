import { createActionObject } from '../../actions';

export const CHANGE_ACCOUNT_MODE = '@/CHANGE_ACCOUNT_MODE';

export const changeAccountMode = (payload: boolean) => {
  return createActionObject(CHANGE_ACCOUNT_MODE, { payload });
};
