import { createActionObject, ErrorProps } from '../../actions';

export const CHECK_HISTORY_REQUEST = '@/CHECK_HISTORY_REQUEST';
export const CHECK_HISTORY_RESPONSE = '@/CHECK_HISTORY_RESPONSE';
export const CHECK_HISTORY_FAIL = '@/CHECK_HISTORY_FAIL';

export const checkHistoryRequest = () => {
  return createActionObject(CHECK_HISTORY_REQUEST);
};

export const checkHistoryResponse = (payload: any) => {
  return createActionObject(CHECK_HISTORY_RESPONSE, { payload });
};

export const checkHistoryFail = (error: ErrorProps) => {
  return createActionObject(CHECK_HISTORY_FAIL, { ...error });
};
