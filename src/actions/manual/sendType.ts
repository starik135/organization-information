import { createActionObject } from '../../actions';

export const SEND_TYPE_MANUAL_REQUEST = '@/SEND_TYPE_MANUAL_REQUEST';

export const sendTypeManualRequest = () => {
  return createActionObject(SEND_TYPE_MANUAL_REQUEST);
};
