import { createActionObject } from '../../actions';

export const DELETE_TYPE_MANUAL_REQUEST = '@/DELETE_TYPE_MANUAL_REQUEST';

export const deleteTypeManualRequest = (payload: number) => {
  return createActionObject(DELETE_TYPE_MANUAL_REQUEST, { payload });
};
