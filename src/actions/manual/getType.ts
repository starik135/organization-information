import { TypeRequest } from '../../entities';
import { createActionObject, ErrorProps } from '../../actions';

export const GET_TYPE_MANUAL_REQUEST = '@/GET_TYPE_MANUAL_REQUEST';
export const GET_TYPE_MANUAL_RESPONSE = '@/GET_TYPE_MANUAL_RESPONSE';
export const GET_TYPE_MANUAL_FAIL = '@/GET_TYPE_MANUAL_FAIL';

export const getTypeManualRequest = () => {
  return createActionObject(GET_TYPE_MANUAL_REQUEST);
};

export const getTypeManualResponse = (payload: TypeRequest) => {
  return createActionObject(GET_TYPE_MANUAL_RESPONSE, { payload });
};

export const getTypeManualFail = (error: ErrorProps) => {
  return createActionObject(GET_TYPE_MANUAL_FAIL, { ...error });
};
