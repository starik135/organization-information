import { createActionObject } from '../../actions';

export const DELETE_USER_MANUAL_REQUEST = '@/DELETE_USER_MANUAL_REQUEST';

export const deleteUserManualRequest = (payload: number) => {
  return createActionObject(DELETE_USER_MANUAL_REQUEST, { payload });
};
