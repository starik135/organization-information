import { UserRequest } from '../../entities';
import { createActionObject, ErrorProps } from '../../actions';

export const GET_USER_MANUAL_REQUEST = '@/GET_USER_MANUAL_REQUEST';
export const GET_USER_MANUAL_RESPONSE = '@/GET_USER_MANUAL_RESPONSE';
export const GET_USER_MANUAL_FAIL = '@/GET_USER_MANUAL_FAIL';

export const getUserManualRequest = () => {
  return createActionObject(GET_USER_MANUAL_REQUEST);
};

export const getUserManualResponse = (payload: UserRequest) => {
  return createActionObject(GET_USER_MANUAL_RESPONSE, { payload });
};

export const getUserManualFail = (error: ErrorProps) => {
  return createActionObject(GET_USER_MANUAL_FAIL, { ...error });
};
