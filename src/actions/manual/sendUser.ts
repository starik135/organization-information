import { createActionObject } from '../../actions';

export const SEND_USER_MANUAL_REQUEST = '@/SEND_USER_MANUAL_REQUEST';
export const SEND_USER_MANUAL_RESPONSE = '@/SEND_USER_MANUAL_RESPONSE';
export const SEND_USER_MANUAL_FAIL = '@/SEND_USER_MANUAL_FAIL';

export const sendUserManualRequest = () => {
  return createActionObject(SEND_USER_MANUAL_REQUEST);
};
