// import { createActionObject, ErrorProps } from '@/../actions';
import { createActionObject, ErrorProps } from '../../actions';
import { OrganizationInfo, OrganizationInfoResponse } from '../../entities';

export const SEND_ORGANIZATION_REQUEST = '@/SEND_ORGANIZATION_REQUEST';
export const SEND_ORGANIZATION_RESPONSE = '@/SEND_ORGANIZATION_RESPONSE';
export const SEND_ORGANIZATION_FAIL = '@/SEND_ORGANIZATION_FAIL';

export const sendOrganizationRequest = (payload: OrganizationInfo) => {
  return createActionObject(SEND_ORGANIZATION_REQUEST, { payload });
};

export const sendOrganizationResponse = (payload: OrganizationInfoResponse) => {
  return createActionObject(SEND_ORGANIZATION_RESPONSE, { payload });
};

export const sendOrganizationFail = (error: ErrorProps) => {
  return createActionObject(SEND_ORGANIZATION_FAIL, { ...error });
};
