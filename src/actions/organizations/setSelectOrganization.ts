// import { createActionObject, ErrorProps } from '@/../actions';
import { createActionObject, ErrorProps } from '../../actions';
import { OrganizationInfoResponse, OrganizationInfo } from '../../entities';

export const SET_SELECTED_ORGANIZATION_REQUEST = '@/SET_SELECTED_ORGANIZATION_REQUEST';
export const SET_SELECTED_ORGANIZATION_RESPONSE = '@/SET_SELECTED_ORGANIZATION_RESPONSE';
export const SET_SELECTED_ORGANIZATION_FAIL = '@/SET_SELECTED_ORGANIZATION_FAIL';

export const setSelectedOrganizationRequest = (payload: OrganizationInfo | number) => {
  return createActionObject(SET_SELECTED_ORGANIZATION_REQUEST, { payload });
};

export const setSelectedOrganizationResponse = (payload: OrganizationInfoResponse) => {
  return createActionObject(SET_SELECTED_ORGANIZATION_RESPONSE, { payload });
};

export const setSelectedOrganizationFail = (error: ErrorProps) => {
  return createActionObject(SET_SELECTED_ORGANIZATION_FAIL, { ...error });
};
