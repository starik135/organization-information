// import { createActionObject, ErrorProps } from '@/../actions';
import { createActionObject, ErrorProps } from '../../actions';

export const NEW_ORGANIZATION_REQUEST = '@/NEW_ORGANIZATION_REQUEST';
export const NEW_ORGANIZATION_FAIL = '@/NEW_ORGANIZATION_FAIL';

export const newOrganizationRequest = () => {
  return createActionObject(NEW_ORGANIZATION_REQUEST);
};

export const newOrganizationFail = (error: ErrorProps) => {
  return createActionObject(NEW_ORGANIZATION_FAIL, { ...error });
};
