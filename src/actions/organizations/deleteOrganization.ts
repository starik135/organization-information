// import { createActionObject, ErrorProps } from '@/../actions';
import { createActionObject, ErrorProps } from '../../actions';

export const DELETE_ORGANIZATION_REQUEST = '@/DELETE_ORGANIZATION_REQUEST';
export const DELETE_ORGANIZATION_FAIL = '@/DELETE_ORGANIZATION_FAIL';

export const deleteOrganizationRequest = (payload: number) => {
  return createActionObject(DELETE_ORGANIZATION_REQUEST, { payload });
};

export const deleteOrganizationFail = (error: ErrorProps) => {
  return createActionObject(DELETE_ORGANIZATION_FAIL, { ...error });
};
