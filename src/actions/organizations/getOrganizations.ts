// import { createActionObject, ErrorProps } from '@/../actions';
import { createActionObject, ErrorProps } from '../../actions';
import { OrganizationInfoResponse } from '../../entities';

export const GET_ORGANIZATIONS_REQUEST = '@/GET_ORGANIZATIONS_REQUEST';
export const GET_ORGANIZATIONS_RESPONSE = '@/GET_ORGANIZATIONS_RESPONSE';
export const GET_ORGANIZATIONS_FAIL = '@/GET_ORGANIZATIONS_FAIL';

export const getOrganizationsRequest = () => {
  return createActionObject(GET_ORGANIZATIONS_REQUEST);
};

export const getOrganizationsResponse = (payload: OrganizationInfoResponse) => {
  return createActionObject(GET_ORGANIZATIONS_RESPONSE, { payload });
};

export const getOrganizationsFail = (error: ErrorProps) => {
  return createActionObject(GET_ORGANIZATIONS_FAIL, { ...error });
};
