import { Action } from 'redux';

export interface ErrorProps {
  error: string;
}

export const createActionObject = (type: string, props?: any): Action => {
  return {
    type,
    ...props,
  };
};
