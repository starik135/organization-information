import { combineReducers/*, Reducer*/ } from 'redux';
import { reducer as formReducer } from 'redux-form';

import usersReducer from './users';
import organizationReducer from './organizations';
import manualReducer from './manual';

// import { RootState } from '../entities';

export const reducers: /*Reducer<RootState>*/any = combineReducers<any/*RootState*/>({
  form: formReducer,
  organization: organizationReducer,
  manual: manualReducer,
  user: usersReducer,
});
