import { combineReducers } from 'redux';

import typeManualReducer from './getType';
import usersManualReducer from './getUser';

export default combineReducers<any>({
  type: typeManualReducer,
  users: usersManualReducer,
});
