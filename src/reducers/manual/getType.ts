import { AnyAction } from 'redux';
import {
  GET_TYPE_MANUAL_REQUEST,
  GET_TYPE_MANUAL_RESPONSE,
  GET_TYPE_MANUAL_FAIL,
} from '../../actions/manual';
import { RequestStatus, CLEAR_USER_DATA } from '../../constants';
import { RequestStatusReducer } from '../../entities';

interface TypeRequest {
  data: string[];
  request: RequestStatusReducer;
}

const initialState: TypeRequest = {
  data: [],
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(state: TypeRequest = initialState, action: AnyAction) {
  switch (action.type) {
    case GET_TYPE_MANUAL_REQUEST:
      return Object.assign({}, state, {
        request: {
          status: RequestStatus.PENDING,
        },
      });

    case GET_TYPE_MANUAL_RESPONSE:
      return Object.assign({}, state, {
        data: action.payload.data,
        request: {
          status: RequestStatus.SUCCESS,
        },
      });

    case GET_TYPE_MANUAL_FAIL:
      return Object.assign({}, state, {
        data: [],
        request: {
          status: RequestStatus.ERROR,
          errorText: action.error,
        },
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default: return state;
  }
}
