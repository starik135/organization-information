import { AnyAction } from 'redux';
import {
  GET_USER_MANUAL_REQUEST,
  GET_USER_MANUAL_RESPONSE,
  GET_USER_MANUAL_FAIL,
} from '../../actions/manual';
import { RequestStatus, CLEAR_USER_DATA } from '../../constants';
import { RequestStatusReducer, UserDate } from '../../entities';

interface UserRequest {
  data: UserDate[];
  request: RequestStatusReducer;
}

const initialState: UserRequest = {
  data: [],
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(state: UserRequest = initialState, action: AnyAction) {
  switch (action.type) {
    case GET_USER_MANUAL_REQUEST:
      return Object.assign({}, state, {
        request: {
          status: RequestStatus.PENDING,
        },
      });

    case GET_USER_MANUAL_RESPONSE:
      return Object.assign({}, state, {
        data: action.payload.data,
        request: {
          status: RequestStatus.SUCCESS,
        },
      });

    case GET_USER_MANUAL_FAIL:
      return Object.assign({}, state, {
        data: [],
        request: {
          status: RequestStatus.ERROR,
          errorText: action.error,
        },
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default: return state;
  }
}
