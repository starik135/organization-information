import { AnyAction } from 'redux';

import { RequestStatus, CLEAR_USER_DATA } from '../../constants';
import {
  GET_ACCOUNT_REQUEST,
  GET_ACCOUNT_RESPONSE,
  GET_ACCOUNT_FAIL,
} from '../../actions/users';

const initialState: any = {
  data: {},
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(state: any = initialState, action: AnyAction) {
  switch (action.type) {
    case GET_ACCOUNT_REQUEST:
      return Object.assign({}, state, {
        request: {
          status: RequestStatus.PENDING,
        },
      });

    case GET_ACCOUNT_RESPONSE:
      return Object.assign({}, state, {
        data: action.payload.data,
        request: {
          status: RequestStatus.SUCCESS,
        },
      });

    case GET_ACCOUNT_FAIL:
      return Object.assign({}, state, {
        data: {},
        request: {
          status: RequestStatus.ERROR,
          errorText: action.error,
        },
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default: return state;
  }
}
