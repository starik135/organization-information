import { AnyAction } from 'redux';

import { CLEAR_USER_DATA } from '../../constants';
import {
  CHANGE_ACCOUNT_MODE,
} from '../../actions/users';

const initialState: any = {
  data: false,
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(state: any = initialState, action: AnyAction) {
  switch (action.type) {
    case CHANGE_ACCOUNT_MODE:
      return Object.assign({}, state, {
        data: action.payload,
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default: return state;
  }
}
