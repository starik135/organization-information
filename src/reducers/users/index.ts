import { combineReducers } from 'redux';

import accountDataReducer from './accountData';
import userModeReducer from './userMode';

export default combineReducers<any>({
  accountData: accountDataReducer,
  editMode: userModeReducer,
});
