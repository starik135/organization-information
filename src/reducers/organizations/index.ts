import { combineReducers } from 'redux';
import organizationInformationReducer from './getData';
import selectOrganizationReducer from './setSelectOrganization';
import { OrganizationReducer } from '../../entities';

export default combineReducers<OrganizationReducer>({
  organizationInformation: organizationInformationReducer,
  selectedOrganization: selectOrganizationReducer,
});
