import { AnyAction } from 'redux';
import { RequestStatus, DEFAULT_ORGANIZATION_FORM, CLEAR_USER_DATA } from '../../constants';
import {
  SET_SELECTED_ORGANIZATION_REQUEST,
  SET_SELECTED_ORGANIZATION_RESPONSE,
  SET_SELECTED_ORGANIZATION_FAIL,
} from '../../actions/organizations';
import { SelectOrganizationResponeData } from '@/entities';

const initialState: SelectOrganizationResponeData = {
  data: {
    id: 0,
    ...DEFAULT_ORGANIZATION_FORM,
  },
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(
  state: SelectOrganizationResponeData = initialState,
  action: AnyAction,
  ) {
  switch (action.type) {
    case SET_SELECTED_ORGANIZATION_REQUEST:
      return Object.assign({}, state, {
        request: {
          status: RequestStatus.PENDING,
        },
      });

    case SET_SELECTED_ORGANIZATION_RESPONSE:
      return Object.assign({}, state, {
        data: action.payload.data,
        request: {
          status: RequestStatus.SUCCESS,
        },
      });

    case SET_SELECTED_ORGANIZATION_FAIL:
      return Object.assign({}, state, {
        data: [],
        request: {
          status: RequestStatus.ERROR,
          errorText: action.error,
        },
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default: return state;
  }
}
