import { AnyAction } from 'redux';
import { RequestStatus, CLEAR_USER_DATA } from '../../constants';
import {
  GET_ORGANIZATIONS_REQUEST,
  GET_ORGANIZATIONS_RESPONSE,
  GET_ORGANIZATIONS_FAIL,
  SEND_ORGANIZATION_FAIL,
  SEND_ORGANIZATION_RESPONSE,
  SEND_ORGANIZATION_REQUEST,
} from '../../actions/organizations';
import { OrganizationResponeData } from '../../entities';

const initialState: OrganizationResponeData = {
  data: [],
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(state: OrganizationResponeData = initialState, action: AnyAction) {
  switch (action.type) {
    case GET_ORGANIZATIONS_REQUEST:
    case SEND_ORGANIZATION_REQUEST:
      return Object.assign({}, state, {
        request: {
          status: RequestStatus.PENDING,
        },
      });

    case GET_ORGANIZATIONS_RESPONSE:
    case SEND_ORGANIZATION_RESPONSE:
      return Object.assign({}, state, {
        data: action.payload.data,
        request: {
          status: RequestStatus.SUCCESS,
        },
      });

    case GET_ORGANIZATIONS_FAIL:
    case SEND_ORGANIZATION_FAIL:
      return Object.assign({}, state, {
        data: [],
        request: {
          status: RequestStatus.ERROR,
          errorText: action.error,
        },
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default: return state;
  }
}
