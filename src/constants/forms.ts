export const SIGN_UP_USER_FORM = 'signUpUserForm';
export const SIGN_UP_USER_MAIL = 'signUpUserMail';
export const SIGN_UP_USER_PASSWORD = 'signUpUserPassword';

export const SIGN_IN_USER_FORM = 'signInUserForm';
export const SIGN_IN_USER_MAIL = 'signInUserMail';
export const SIGN_IN_USER_PASSWORD = 'signInUserPassword';

export const ORGANIZATION_ID = 'id';
export const ORGANIZATION_INFORMATION_FORM = 'OrganizationInformation';
export const ORGANIZATION_NAME = 'organizationName';
export const NOTES = 'notes';
export const ADDRESS_ORGANIZATION = 'addressOrganization';
export const PHONE_NUMBER = 'phoneNumber';
export const COUNT_EMPLOYEES = 'countEmployee';
export const ADDITIONAL_INFORMATION = 'additionalInformation';
export const TYPE_OF_ORGANIZATION = 'typeOfOrganization';
export const CHIEF_NAME = 'chiefName';
export const CALENDAR_ORGANIZATION = 'calendarOrganization';

export const ADD_TYPE_FORM = 'AddTypeForm';
export const ADD_TYPE = 'addType';

export const ADD_USER_FORM = 'AddUserForm';
export const ADD_USER_NAME = 'addUserName';
export const ADD_USER_LAST_NAME = 'addUserLastName';
export const ADD_USER_PATRONYMIC = 'addUserPatronymic';

export const ACCOUNT_LOGIN_FORM = 'accountLoginForm';
export const ACCOUNT_LOGIN_MAIL = 'loginMail';
export const ACCOUNT_LOGIN_PASSWORD = 'loginPassword';

export const ACCOUNT_USER_FORM = 'accountUserForm';
export const ACCOUNT_USER_FIRST_NAME = 'userFirstName';
export const ACCOUNT_USER_LAST_NAME = 'userLastName';
export const ACCOUNT_USER_PATRONYMIC = 'userPatronymic';

export const PHONE_NUMBER_LENGTH = 9;

export const FIELD_TYPE_TEXT = 'text';
export const FIELD_TYPE_PASSWORD = 'password';
