export const SIGN_FORM_APP_ENDPOINT = '/sign-form';
export const ORGANIZATIONS_APP_ENDPOINT = '/organizations';
export const MANUALS_APP_ENDPOINT = '/manuals';
export const ACCOUNT_APP_ENDPOINT = '/account';
