import * as moment from 'moment';

export const RequestStatus = {
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

export const ADD_DATE = '@/ADD_DATE';
export const SELECTED_TYPE_MANUAL = '@/SELECTED_TYPE_MANUAL';
export const CLEAR_USER_DATA = '@/CLEAR_USER_DATA';

export const ORGANIZATION_INFORMATION = 'organization-infomation';

export const CALENDAR_FORMAT = 'MM/DD/YYYY';
export const CURRENT_DATA = moment().format(CALENDAR_FORMAT);

export const LARGE = 'large';

export const TYPE_ORGANIZATION = 'TYPE_ORGANIZATION';
export const USER = 'USER';

export const TYPE_FORM = 'typeForm';
export const USER_FORM = 'userFrom';

export const DEFAULT_ORGANIZATION_FORM = {
  addressOrganization: '',
  countEmployee: '',
  notes: '',
  organizationName: '',
  phoneNumber: '',
  additionalInformation: '',
  calendarOrganization: '',
  chiefName: '',
  typeOfOrganization: '',
};
