export interface RootState {
  form: any;
  organization: OrganizationReducer;
  manual: any;
  user: any;
}

export interface OrganizationReducer {
  organizationInformation: OrganizationResponeData;
  selectedOrganization: SelectOrganizationResponeData;
}

export interface OrganizationInfo {
  addressOrganization: string;
  countEmployee: string;
  id: number;
  notes: string;
  organizationName: string;
  phoneNumber: string;
  typeOfOrganization: string;
  chiefName: string;
  additionalInformation: string;
  calendarOrganization: string;
}

export interface OrganizationInfoResponse {
  data: OrganizationInfo[];
}

export interface RequestStatusInterface {
  errorText: string;
  status: string;
}

export interface SelectOrganizationResponeData {
  data: OrganizationInfo;
  request: RequestStatusInterface;
}

export interface OrganizationResponeData {
  data: OrganizationInfo[];
  request: RequestStatusInterface;
}

export interface OrganizationDate {
  data: string;
}

export interface UserDate {
  firstName: string;
  lastName: string;
  patronymic: string;
}

export interface UserRequest {
  data: UserDate;
}

export interface TypeRequest {
  data: {
    data: string[];
  };
}

export interface TableConfig {
  key: string;
  name: string;
}

export interface ValidateTypesManual {
  addType: string;
}

export interface ValidateUsersManual {
  addUserName: string;
  addUserLastName: string;
  addUserPatronymic: string;
}

export interface RequestStatusReducer {
  errorText: string;
  status: string;
}
