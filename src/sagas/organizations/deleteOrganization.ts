import * as firebase from 'firebase/app';
import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  deleteOrganizationFail,
  DELETE_ORGANIZATION_REQUEST,
  getOrganizationsRequest,
  setSelectedOrganizationRequest,
} from '../../actions/organizations';
import { OrganizationInfo } from '../../entities';

function* deleteOrganization(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/organization');

    yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataOrganization: OrganizationInfo[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataOrganization.push(childSnapshot.val());
        });

        return Promise.all(dataOrganization);
      })
      .then((res: OrganizationInfo[]) => {
        const filterData = res
          .filter((item: OrganizationInfo) => item.id !== action.payload)
          .map((item: OrganizationInfo, index: number) => ({ ...item, id: index }));

        firebaseRef.set({ ...filterData });
      });

    yield put(getOrganizationsRequest());
    yield put(setSelectedOrganizationRequest(0));
  } catch (error) {
    yield put(deleteOrganizationFail(error));
  }
}

export function* deleteOrganizationSaga() {
  yield takeEvery(DELETE_ORGANIZATION_REQUEST, deleteOrganization);
}
