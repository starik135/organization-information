import * as firebase from 'firebase';
import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  getOrganizationsFail,
  getOrganizationsResponse,
  GET_ORGANIZATIONS_REQUEST,
} from '../../actions/organizations';

import { OrganizationInfo } from '../../entities';
// import { DEFAULT_ORGANIZATION_FORM } from '../../constants';

function* getData(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/organization');

    const data = yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataOrganization: OrganizationInfo[] = [];
        snapshot.forEach((childSnapshot: any) => {
          dataOrganization.push(childSnapshot.val());
        });

        return Promise.all(dataOrganization);
      });

    yield put(getOrganizationsResponse({ data }));
  } catch (error) {
    yield put(getOrganizationsFail(error));
  }
}

export function* getDataSaga() {
  yield takeEvery(GET_ORGANIZATIONS_REQUEST, getData);
}
