import * as firebase from 'firebase';

import { isObject } from 'lodash';
import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  setSelectedOrganizationFail,
  SET_SELECTED_ORGANIZATION_REQUEST,
  setSelectedOrganizationResponse,
} from '../../actions/organizations';
import { OrganizationInfo } from '../../entities';

function* setSelectedOrganization(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/organization');

    if (isObject(action.payload)) {
      yield put(setSelectedOrganizationResponse({ data: action.payload }));
    } else {
      const res = yield firebaseRef
        .once('value')
        .then((snapshot: any) => {
          const dataOrganization: OrganizationInfo[] = [];
          snapshot.forEach((childSnapshot: any) => {
            dataOrganization.push(childSnapshot.val());
          });

          return Promise.all(dataOrganization);
        })
        .then((res: OrganizationInfo[]) => {
          const data = res.filter((item: OrganizationInfo) => `${item.id}` === `${action.payload}`);

          return Promise.all(data);
        });

      yield put(setSelectedOrganizationResponse({ data: res[0] }));
    }
  } catch (error) {
    yield put(setSelectedOrganizationFail(error));
  }
}

export function* setSelectedOrganizationSaga() {
  yield takeEvery(SET_SELECTED_ORGANIZATION_REQUEST, setSelectedOrganization);
}
