import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { notification } from 'antd';
import { takeEvery, put } from 'redux-saga/effects';

import {
  newOrganizationFail,
  getOrganizationsRequest,
  NEW_ORGANIZATION_REQUEST,
  setSelectedOrganizationRequest,
} from '../../actions/organizations';

import { loc } from '../../localization';
import { OrganizationInfo } from '../../entities';
import { DEFAULT_ORGANIZATION_FORM } from '../../constants';

function* newOrganization(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/organization');

    const data = yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataOrganization: OrganizationInfo[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataOrganization.push(childSnapshot.val());
        });

        return Promise.all(dataOrganization);
      })
      .then((res: OrganizationInfo[]) => {
        res.map((item: OrganizationInfo) => {
          if (item.organizationName.length === 0) {
            notification.error({
              message: loc.organizationInformationForm.notificationTitle,
              description: loc.errorThrow.existsEmptyForm,
            });
            throw Error(loc.errorThrow.emptyForm);
          }
        });

        res.push({
          ...DEFAULT_ORGANIZATION_FORM,
          id: res.length,
        });

        firebase.database().ref('/organization').update({ ...res });
        return Promise.all(res);
      });

    yield put(getOrganizationsRequest());
    yield put(setSelectedOrganizationRequest(data.length - 1));
  } catch (error) {
    yield put(newOrganizationFail(error));
  }
}

export function* newOrganizationSaga() {
  yield takeEvery(NEW_ORGANIZATION_REQUEST, newOrganization);
}
