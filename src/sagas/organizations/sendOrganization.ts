import * as firebase from 'firebase';
import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  sendOrganizationFail,
  SEND_ORGANIZATION_REQUEST,
  getOrganizationsRequest,
  setSelectedOrganizationRequest,
} from '../../actions/organizations';

function* sendOrganization(action: AnyAction) {
  try {
    const currentId = action.payload.id;
    const firebaseRef = firebase.database().ref('/organization');

    yield firebaseRef
      .once('value')
      .then(() => {
        firebase.database().ref(`/organization/${currentId}`).update({ ...action.payload });
      });

    yield put(getOrganizationsRequest());
    yield put(setSelectedOrganizationRequest(currentId));
  } catch (error) {
    yield put(sendOrganizationFail(error));
  }
}

export function* sendOrganizationSaga() {
  yield takeEvery(SEND_ORGANIZATION_REQUEST, sendOrganization);
}
