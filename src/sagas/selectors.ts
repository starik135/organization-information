import { RootState } from '../entities';

export const getState = (state: RootState): RootState => state;
