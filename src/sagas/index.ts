import { fork, all } from 'redux-saga/effects';

import * as users from './users';
import * as organizations from './organizations';
import * as manual from './manual';
import * as chat from './chat';

function* rootSaga() {
  yield all([
    fork(users.currentUserSaga),
    fork(users.registerUserSaga),
    fork(users.getAccountSaga),
    fork(users.changeLoginSaga),
    fork(users.changeUserSaga),

    fork(organizations.getDataSaga),
    fork(organizations.sendOrganizationSaga),
    fork(organizations.setSelectedOrganizationSaga),
    fork(organizations.deleteOrganizationSaga),
    fork(organizations.newOrganizationSaga),

    fork(manual.getTypeManualSaga),
    fork(manual.sendTypeManualSaga),
    fork(manual.deleteTypeManualSaga),
    fork(manual.getUserManualSaga),
    fork(manual.sendUserManualSaga),
    fork(manual.deleteUserManualSaga),

    fork(chat.checkHistorySaga),
  ]);
}

export default rootSaga;
