import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  checkHistoryFail,
  CHECK_HISTORY_REQUEST,
} from '../../actions/chat';

function* checkHistory(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/rooms');

    yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: string[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      })
      .then((res: any) => console.log(res));

  } catch (error) {
    yield put(checkHistoryFail(error));
  }
}

export function* checkHistorySaga() {
  yield takeEvery(CHECK_HISTORY_REQUEST, checkHistory);
}
