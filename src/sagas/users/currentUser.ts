import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { notification } from 'antd';
import { formValueSelector } from 'redux-form';
import { takeEvery, put, select } from 'redux-saga/effects';

import { getState } from '../selectors';
import { loc } from '../../localization';

import {
  currentUserFail,
  CURRENT_USER_REQUEST,
} from '../../actions/users';
import {
  SIGN_IN_USER_FORM,
  SIGN_IN_USER_MAIL,
  SIGN_IN_USER_PASSWORD,
  ORGANIZATIONS_APP_ENDPOINT,
} from '../../constants';

function* currentUser(action: AnyAction) {
  try {
    const history = action.payload;
    const state = yield select(getState);
    const userSelector = formValueSelector(SIGN_IN_USER_FORM);
    const email = userSelector(state, SIGN_IN_USER_MAIL);
    const password = userSelector(state, SIGN_IN_USER_PASSWORD);

    yield firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        notification.success({
          message: loc.login.successfully,
          description: loc.login.welcome,
        });
      })
      .catch((error: any) => {
        notification.error({
          message: loc.errorThrow.userNotExist,
          description: error.message,
        });

        throw Error(loc.errorThrow.userNotExist);
      });

    // @todo: fix
    if (history.push(ORGANIZATIONS_APP_ENDPOINT)) {
      yield put(history.push(ORGANIZATIONS_APP_ENDPOINT));
    }
  } catch (error) {
    yield put(currentUserFail(error));
  }
}

export function* currentUserSaga() {
  yield takeEvery(CURRENT_USER_REQUEST, currentUser);
}
