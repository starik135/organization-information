import * as firebase from 'firebase';

import { isNull } from 'util';
import { AnyAction } from 'redux';
import { formValueSelector } from 'redux-form';
import { takeEvery, put, select } from 'redux-saga/effects';

import {
  changeUserFail,
  CHANGE_USER_REQUEST,
  getAccountResponse,
} from '../../actions/users';
import { getState } from '../selectors';
import {
  ACCOUNT_USER_FORM,
  ACCOUNT_USER_PATRONYMIC,
  ACCOUNT_USER_FIRST_NAME,
  ACCOUNT_USER_LAST_NAME,
} from '../../constants';

function* changeUser(action: AnyAction) {
  try {
    const state = yield select(getState);
    const userSelector = formValueSelector(ACCOUNT_USER_FORM);
    const firstName = userSelector(state, ACCOUNT_USER_FIRST_NAME) || '';
    const lastName = userSelector(state, ACCOUNT_USER_LAST_NAME) || '';
    const patronymic = userSelector(state, ACCOUNT_USER_PATRONYMIC) || '';

    const data = yield new Promise((res, rej) => {
      const user = firebase.auth().currentUser;

      if (!isNull(user)) {

        user.updateProfile({
          displayName: `${lastName} ${firstName} ${patronymic}`,
          photoURL: '',
        });

        res({
          lastName,
          firstName,
          patronymic,
          id: user.uid,
          email: user.email,
        });
      }
    });

    yield put(getAccountResponse({ data }));
  } catch (error) {
    yield put(changeUserFail(error));
  }
}

export function* changeUserSaga() {
  yield takeEvery(CHANGE_USER_REQUEST, changeUser);
}
