import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { formValueSelector } from 'redux-form';
import { takeEvery, put, select } from 'redux-saga/effects';
import { notification } from 'antd';

import { getState } from '../selectors';

import {
  REGISTER_USER_REQUEST,
  registerUserFail,
} from '../../actions/users';
import {
  SIGN_UP_USER_FORM,
  SIGN_UP_USER_MAIL,
  SIGN_UP_USER_PASSWORD,
  SIGN_FORM_APP_ENDPOINT,
} from '../../constants';

function* registerUser(action: AnyAction) {
  try {
    const history = action.payload;

    const state = yield select(getState);
    const userSelector = formValueSelector(SIGN_UP_USER_FORM);

    const email = userSelector(state, SIGN_UP_USER_MAIL);
    const password = userSelector(state, SIGN_UP_USER_PASSWORD);

    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .catch((error: any) => {
        const errorCode = error.code;
        const errorMessage = error.message;

        notification.error({
          message: errorCode,
          description: errorMessage,
        });
      });

    if (history.push(SIGN_FORM_APP_ENDPOINT)) {
      yield put(history.push(SIGN_FORM_APP_ENDPOINT));
    }
  } catch (error) {
    yield put(registerUserFail(error));
  }
}

export function* registerUserSaga() {
  yield takeEvery(REGISTER_USER_REQUEST, registerUser);
}
