import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  CHANGE_LOGIN_REQUEST,
  changeLoginFail,
} from '../../actions/users';

function* changeLogin(action: AnyAction) {
  try {
    yield new Promise((res, rej) => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          res({
            email: user.email,
            id: user.uid,
            firstName: '',
            lastName: '',
            patronymic: '',
          });
        } else {
          rej('Unauthorization user');
        }
      });
    });

  } catch (error) {
    yield put(changeLoginFail(error));
  }
}

export function* changeLoginSaga() {
  yield takeEvery(CHANGE_LOGIN_REQUEST, changeLogin);
}
