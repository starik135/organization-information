import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import { GET_ACCOUNT_REQUEST, getAccountFail, getAccountResponse } from '../../actions/users';

function* getAccount(action: AnyAction) {
  try {
    const data = yield new Promise((res, rej) => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          let userName: any = [];

          if (user.displayName !== null) {
            userName = user.displayName.split(' ');
          }

          res({
            email: user.email,
            id: user.uid,
            lastName: userName[0],
            firstName: userName[1],
            patronymic: userName[2],
          });
        } else {
          rej('Unauthorization user');
        }
      });
    });

    yield put(getAccountResponse({ data }));
  } catch (error) {
    yield put(getAccountFail(error));
  }
}

export function* getAccountSaga() {
  yield takeEvery(GET_ACCOUNT_REQUEST, getAccount);
}
