import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  getTypeManualFail,
  getTypeManualResponse,
  GET_TYPE_MANUAL_REQUEST,
} from '../../actions/manual';

function* getTypeManual(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/types');

    const data = yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: string[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      });

    yield put(getTypeManualResponse({ data }));
  } catch (error) {
    yield put(getTypeManualFail(error));
  }
}

export function* getTypeManualSaga() {
  yield takeEvery(GET_TYPE_MANUAL_REQUEST, getTypeManual);
}
