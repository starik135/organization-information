import * as firebase from 'firebase/app';

import { AnyAction } from 'redux';
import { notification } from 'antd';
import { takeEvery, put } from 'redux-saga/effects';

import {
  getTypeManualRequest,
  DELETE_TYPE_MANUAL_REQUEST,
} from '../../actions/manual';

function* deleteTypeManual(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/types');

    yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: string[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      })
      .then((res: string[]) => {
        const newData = res.filter((item: string, index: number) => index !== action.payload);
        firebaseRef.set(newData);
      });

    yield put(getTypeManualRequest());
  } catch (error) {
    notification.error({
      message: error.name,
      description: error.message,
    });
  }
}

export function* deleteTypeManualSaga() {
  yield takeEvery(DELETE_TYPE_MANUAL_REQUEST, deleteTypeManual);
}
