import * as firebase from 'firebase/app';

import { AnyAction } from 'redux';
import { notification } from 'antd';
import { takeEvery, put } from 'redux-saga/effects';

import {
  getUserManualRequest,
  DELETE_USER_MANUAL_REQUEST,
} from '../../actions/manual';
import { UserDate } from '../../entities';

function* deleteUserManual(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/users');

    yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: UserDate[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      })
      .then((res: UserDate[]) => {
        const newData = res.filter((item: UserDate, index: number) => index !== action.payload);
        firebaseRef.set(newData);
      });

    yield put(getUserManualRequest());
  } catch (error) {
    notification.error({
      message: error.name,
      description: error.message,
    });
  }
}

export function* deleteUserManualSaga() {
  yield takeEvery(DELETE_USER_MANUAL_REQUEST, deleteUserManual);
}
