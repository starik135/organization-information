import * as firebase from 'firebase';

import { notification } from 'antd';
import { formValueSelector } from 'redux-form';
import { takeEvery, put, select } from 'redux-saga/effects';

import {
  getTypeManualRequest,
  SEND_TYPE_MANUAL_REQUEST,
} from '../../actions/manual';
import { getState } from '../selectors';
import { loc } from '../../localization';
import { ADD_TYPE_FORM, ADD_TYPE } from '../../constants';

function* sendTypeManual() {
  try {
    const state = yield select(getState);
    const typeSelector = formValueSelector(ADD_TYPE_FORM);
    const type = typeSelector(state, ADD_TYPE);
    const firebaseRef = firebase.database().ref('/types');

    yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: string[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      })
      .then((result: string[]) => {
        result
          .map((item: string) => {
            if (item === type) {
              notification.error({
                message: loc.manualsName.typesNotificationTitle,
                description: loc.errorThrow.exists,
              });
              throw Error(loc.errorThrow.exists);
            }
          })
          .push(type);

        firebaseRef.set(result);
      });

    notification.success({
      message: loc.manualsName.typesNotificationTitle,
      description: loc.manualsName.typesNotification,
    });

    yield put(getTypeManualRequest());
  } catch (error) {
    notification.error({
      message: error.name,
      description: error.message,
    });
  }
}

export function* sendTypeManualSaga() {
  yield takeEvery(SEND_TYPE_MANUAL_REQUEST, sendTypeManual);
}
