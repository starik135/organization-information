import * as firebase from 'firebase';

import { isEqual } from 'lodash';
import { notification } from 'antd';
import { formValueSelector } from 'redux-form';
import { takeEvery, put, select  } from 'redux-saga/effects';

import {
  ADD_USER_FORM,
  ADD_USER_NAME,
  ADD_USER_LAST_NAME,
  ADD_USER_PATRONYMIC,
} from '../../constants';

import {
  getUserManualRequest,
  SEND_USER_MANUAL_REQUEST,
} from '../../actions/manual';

import { getState } from '../selectors';
import { loc } from '../../localization';
import { UserDate } from '../../entities';

function* sendUserManual() {
  try {
    const state = yield select(getState);
    const userSelector = formValueSelector(ADD_USER_FORM);
    const firebaseRef = firebase.database().ref('/users');

    const sendObject: any = {
      firstName: userSelector(state, ADD_USER_NAME),
      lastName: userSelector(state, ADD_USER_LAST_NAME),
      patronymic: userSelector(state, ADD_USER_PATRONYMIC),
    };

    yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: UserDate[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      })
      .then((result: UserDate[]) => {

        result
          .map((item: UserDate) => {
            if (isEqual(item, sendObject)) {
              notification.error({
                message: loc.manualsName.typesNotificationTitle,
                description: loc.errorThrow.userExists,
              });
              throw Error(loc.errorThrow.userExists);
            }
          })
          .push(sendObject);

        firebaseRef.set(result);
      });

    notification.success({
      message: loc.manualsName.usersNotificationTitle,
      description: loc.manualsName.usersNotification,
    });

    yield put(getUserManualRequest());
  } catch (error) {
    notification.error({
      message: error.name,
      description: error.message,
    });
  }
}

export function* sendUserManualSaga() {
  yield takeEvery(SEND_USER_MANUAL_REQUEST, sendUserManual);
}
