import * as firebase from 'firebase';

import { AnyAction } from 'redux';
import { takeEvery, put } from 'redux-saga/effects';

import {
  getUserManualFail,
  getUserManualResponse,
  GET_USER_MANUAL_REQUEST,
} from '../../actions/manual';
import { UserDate } from '../../entities';

function* getUserManual(action: AnyAction) {
  try {
    const firebaseRef = firebase.database().ref('/users');

    const data = yield firebaseRef
      .once('value')
      .then((snapshot: any) => {
        const dataTypes: UserDate[] = [];

        snapshot.forEach((childSnapshot: any) => {
          dataTypes.push(childSnapshot.val());
        });

        return Promise.all(dataTypes);
      });

    yield put(getUserManualResponse({ data }));
  } catch (error) {
    yield put(getUserManualFail(error));
  }
}

export function* getUserManualSaga() {
  yield takeEvery(GET_USER_MANUAL_REQUEST, getUserManual);
}
