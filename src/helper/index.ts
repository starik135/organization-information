import * as firebase from 'firebase';

import { UserDate } from '../entities';
import { SIGN_FORM_APP_ENDPOINT } from '../constants';

export const randomString = require('nanoid');

export const normalizePhone = (value: string) => {
  if (!value) return value;

  const onlyNums = value.replace(/[^\d]/g, '');

  if (onlyNums.length <= 3) return onlyNums;
  if (onlyNums.length <= 5) return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`;

  return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3, 5)}-${onlyNums.slice(5, 7)}`;
};

export const normalizeCountEmployees = (value: string) => {
  return value ? value.replace(/[^\d]/g, '') : value;
};

export const findValue = (data: UserDate[], value: string) => {
  return data
    .map((item: UserDate) => `${item.lastName} ${item.firstName} ${item.patronymic}`)
    .filter((item: string) => item.toLowerCase().indexOf(value.toLowerCase()) !== -1)
    .map((item: string) => {
      return {
        lastName: item.split(' ')[0],
        firstName: item.split(' ')[1],
        patronymic: item.split(' ')[2],
      };
    });
};

export const filterList = (data: string[], value: string) => {
  return data.filter((item: string) => item.toLowerCase().indexOf(value.toLowerCase()) !== -1);
};

export const isUserAuth = (history: any) => {
  firebase.auth().onAuthStateChanged((user: any) => {
    !user && history.push(SIGN_FORM_APP_ENDPOINT);
  });
};
