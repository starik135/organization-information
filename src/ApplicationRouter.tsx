import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import {
  OrganizationsPage,
  SignPage,
  ManualsPage,
  AccountPage,
} from './components/pages';

import {
  SIGN_FORM_APP_ENDPOINT,
  ACCOUNT_APP_ENDPOINT,
  MANUALS_APP_ENDPOINT,
  ORGANIZATIONS_APP_ENDPOINT,
} from './constants/index';

export default class ApplicationRouter extends React.Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <Route path={SIGN_FORM_APP_ENDPOINT} component={SignPage} exact />
          <Route path={ACCOUNT_APP_ENDPOINT} component={AccountPage} exact />
          <Route path={ORGANIZATIONS_APP_ENDPOINT} component={OrganizationsPage} exact />
          <Route path={MANUALS_APP_ENDPOINT} component={ManualsPage} exact />
        </React.Fragment>
      </Router>
    );
  }
}
