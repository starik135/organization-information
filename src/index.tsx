import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as firebase from 'firebase/app';
import { Provider } from 'react-redux';

import { store } from './store';
import { firebaseConfig } from './constants';
import { persistStore } from 'redux-persist';
import ApplicationRouter from './ApplicationRouter';

import './styles/styles';

const reactPersist = require('redux-persist/integration/react');

const PersistGate = reactPersist.PersistGate;
const persistor = persistStore(store);

firebase.initializeApp(firebaseConfig);
class Application extends React.Component {
  public componentWillMount() {
    persistStore(store, undefined, () => {
      store.getState();
    });
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
          <ApplicationRouter />
        </PersistGate>
      </Provider>
    );
  }
}

ReactDOM.render(
  <Application />,
  document.getElementById('root'),
);
