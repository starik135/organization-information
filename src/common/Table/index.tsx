import * as React from 'react';
import classnames from 'classnames';

import { randomString } from '../../helper';
import { TableConfig, UserDate } from '../../entities';

import {
  AllProps,
  ComponentState,
} from './types';

import './styles.scss';

class Table extends React.Component<AllProps, ComponentState> {
  constructor(props: AllProps) {
    super(props);

    this.state = {
      selectId: 0,
    };
  }

  private renderHeader = (tableConfig: TableConfig[]) => (
    <tr>{tableConfig.map(columnName => <th key={randomString()}>{columnName.name}</th>)}</tr>
  )

  private selectItem = (id: number) => {
    this.props.getSelectedId(id);
    this.setState({ selectId: id });
  }

  private renderBody = (data: UserDate[], tableConfig: TableConfig[], selectId: number) => {
    return data.map((rowItem: UserDate, index: number) => (
      <tr
        key={randomString()}
        className={classnames({
          'list-item': true,
          'select-item': selectId === index,
        })}
        onClick={() => this.selectItem(index)}
      >
        {
          tableConfig.map((columnItem: TableConfig) => (
            <td key={randomString()}>
              {rowItem[columnItem.key]}
            </td>
          ))
        }
      </tr>
    ));
  }

  render() {
    const { tableConfig, data, className } = this.props;
    const { selectId } = this.state;

    return (
      <table className={className}>
        <thead>
          {this.renderHeader(tableConfig)}
        </thead>
        <tbody>
          {this.renderBody(data, tableConfig, selectId)}
        </tbody>
      </table>
    );
  }
}

export default Table;
