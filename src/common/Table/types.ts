import { TableConfig, UserDate } from '../../entities';

export interface ComponentProps {
  tableConfig: TableConfig[];
  data: UserDate[];
  className?: string;
  getSelectedId: (id: number) => void;
}

export interface ComponentState {
  selectId: number;
}

export interface MapStateProps { }

export interface MapDispatchProps { }

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps;
