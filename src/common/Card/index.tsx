import * as React from 'react';

import './styles';

export const Card = (props: any) => (
  <div
    className="card-wrapper"
    style={{
      width: props.width || 400,
      height: props.height || 400,
      padding: props.padding || 30,
      borderRadius: props.borderRadius || 50,
      borderColor: props.borderColor,
    }}
  >
    {props.children}
  </div>
);
